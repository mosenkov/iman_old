import matplotlib.pyplot as plt
from astropy.visualization import make_lupton_rgb
from astropy.io import fits
from astropy.utils.data import get_pkg_data_filename



# Read in the three images downloaded from here:
g_name = '/home/amosenko/MyWork_TODO/PRGs_envelopes/Maria_results/results_Maria/Maria_data_preparation/SPRC185_S82_09g_161/galaxy.fits'
r_name = '/home/amosenko/MyWork_TODO/PRGs_envelopes/Maria_results/results_Maria/Maria_data_preparation/SPRC185_S82_09r_161/galaxy.fits'
i_name = '/home/amosenko/MyWork_TODO/PRGs_envelopes/Maria_results/results_Maria/Maria_data_preparation/SPRC185_S82_09i_161/galaxy.fits'


def main(g_name, r_name, i_name, output_file):
    g = fits.open(g_name)[0].data
    r = fits.open(r_name)[0].data
    i = fits.open(i_name)[0].data

    rgb_default = make_lupton_rgb(i, r, g, filename=output_file)
    plt.imshow(rgb_default, origin='lower')

    #plt.savefig(output_file, bbox_inches='tight', pad_inches=0)
    #plt.clf()
    #plt.close()

output_file = 'test.jpg'

main(g_name, r_name, i_name, output_file)