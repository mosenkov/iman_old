#!/usr/bin/python
# -*- coding:  cp1251 -*-
# *****************************************************************
# **       IMP -- Python Toolkit for image preparation           **
# **          to olygochromatic (multiband) fitting              **
# **           © Saint Petersburg State University               **
# **         Written by S. Savchenko and A. Mosenkov             **
# **      savchenko.s.s@gmail.com, mosenkovAV@gmail.com          **
# *****************************************************************
# Description: Script to determine PSF (using Moffat function). Several frames of the same object can be downloaded. The astrometrically corrected fits-images should be placed in the /raw directory. Only fits files should be found there!
# Type for help: $python imp.py -h

# Import standard modules
import sys
import math
import numpy as np
from scipy import stats
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import matplotlib.path as path
from matplotlib.ticker import NullFormatter
from numpy import *
from pylab import *
import os
import shutil
import subprocess
import random
import re
import glob
import argparse
import pyfits


import imp_rebin
import imp_skysub
import imp_rotate
import imp_center
import imp_masking
import imp_psf
import imp_zp
import imp_setup

#*** Colour fonts ***
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''
        
PATH_TO_SCRIPT = os.path.dirname(os.path.realpath(__file__))        

def remove_files(files):
  for file in files:
    if os.path.exists(file):
	os.remove(file)    

def delete_tmp(except_file):
  print 'Removing temporary files'
  arr = glob.glob('*.*')

  files = []
  for ar in arr:
    if ar!=except_file:
      files.append(ar)
  remove_files(files)


def ask_input_dir():
    N_dir = 0
    print '\n\n Directories:'
    directories = glob.glob("*/")
    for k in range(len(directories)):
      N_dir = N_dir + 1
      print '\t%i. %s' % (N_dir,directories[k])
    
    directory = str(raw_input('Please choose the directory (number) to work with: %i? ' % (1)) or '1')
    directory = int(directory) - 1 # index
    directory = directories[directory]
    return directory

  
def main(f_out,steps,modes,pix2sec2,m0,xc,yc,Radius,band,input_directory='/raw'):
  if not os.path.exists("./reg"):
    os.makedirs("./reg")

  steps = steps.split(',')
  modes = modes.split(',')

  #if pix2sec2!=None and pix2sec2!=1. and 0 not in steps:
  #  steps = [0] + steps
  #  modes = ['m'] + modes
  #print steps
  #exit()
  for step in steps:
    k = steps.index(step)
    mode = modes[k]
    best_res_inp = pix2sec2

    if step=='-2':
      # Astrometry
      os.chdir(input_directory)
      arr = glob.glob('*.fits')
      for ar in arr:
	print bcolors.OKGREEN+ar+bcolors.ENDC
	subprocess.call('%s/autoastrometry.py %s -d 600 -b 600' % (PATH_TO_SCRIPT,ar), shell=True)
	if os.path.exists('match.list'):
	  remove_files(['cat.txt','det.im.reg','det.wcs.txt','matchlines.wcs.reg','sex.config','temp.cat',\
		 'cat.wcs.reg','det.init.txt','matchlines.im.reg','match.list','sex.conv','temp.fits'])
	  shutil.move('a'+ar,ar)
	else:
	  print bcolors.FAIL+'Astrometry failed!'+ bcolors.ENDC
	  exit()
      os.chdir('..')
    if step=='-1':
      # Crop from the field
      arr = glob.glob(input_directory+'/*.fits')
      for ar in arr:
	  HDU = pyfits.open(ar)
	  data = HDU[0].data
	  header = HDU[0].header
	  ny,nx = data.shape
    
	  from astropy.wcs import WCS
	  w = WCS(ar)
	  [[x_obj,y_obj]] = w.wcs_world2pix([[xc,yc]], 1)

	  x_l = x_obj - imp_setup.Ini_crop_factor*Radius/pix2sec2
	  y_l = y_obj - imp_setup.Ini_crop_factor*Radius/pix2sec2
	  x_r = x_obj + imp_setup.Ini_crop_factor*Radius/pix2sec2
	  y_r = y_obj + imp_setup.Ini_crop_factor*Radius/pix2sec2
	  if x_l<0:
	    x_l = 0
	  if y_l<0:
	    y_l = 0
	  if x_r>nx-1:
	    x_r = nx - 1
	  if y_r>ny-1:
	    y_r = ny - 1
	  imp_center.crop(ar,ar,x_l,y_l,x_r,y_r)
    if step=='0':
      # Rebinning
      input_dir = ask_input_dir()

      if mode!='m':
	imp_rebin.main(inp_dir = input_dir, mode = mode, outp_dir = './rebin', best_res_inp = None)
      else:
	imp_rebin.main(inp_dir = input_dir, mode = mode, outp_dir = './rebin', best_res_inp = float(best_res_inp[0]))
    if step=='1':
      # Sky-subtraction
      input_dir = ask_input_dir()
      imp_skysub.main(inp_dir = input_dir, mode = mode, outp_dir = './sky_subtr', xc=xc, yc=yc)
    if step=='2':
      # Rotation
      input_dir = ask_input_dir()
      imp_rotate.main(inp_dir = input_dir, mode = mode, outp_dir="./rot", ref_image=None, x_obj=xc, y_obj=yc, coords='world', m0=m0, pix2sec=pix2sec2)
    if step=='3':
      # Centering
      input_dir = ask_input_dir()
      imp_center.main(inp_dir = input_dir, mode = mode, outp_dir="./ext", ref_image=None, x_obj=xc, y_obj=yc, coords='world', m0=m0, pix2sec=pix2sec2)
    if step=='4':
      # Masking
      input_dir = ask_input_dir()
      imp_masking.main(inp_dir = input_dir, mode = mode, outp_dir="./mask", ref_image=None)
    if step=='5':
      # PSF determination
      input_dir = ask_input_dir()
      imp_psf.main(inp_dir = input_dir, mode=mode,pix2sec=pix2sec2)
    if step=='6':
      # Zero point
      imp_zp.main(band)
    if step=='7':
      # Normalization
      input_dir = ask_input_dir()
      imp_masking.normalization(inp_dir=input_dir, outp_dir="./norm")
    delete_tmp('zero_point.txt')
    print 'Done!'