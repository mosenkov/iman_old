#!/usr/bin/python
# -*- coding:  cp1251 -*-
import sys
import math
import numpy as np
from scipy import stats
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import matplotlib.path as path
from matplotlib.ticker import NullFormatter
from numpy import *
from pylab import *
import os
import shutil
import subprocess
import random
import re
import glob
import pyfits
import collections
from skimage import measure
import pyfits
from skimage.morphology import skeletonize
from photutils import find_peaks
from skimage.transform import (hough_line, hough_line_peaks,
                               probabilistic_hough_line)
from skimage.feature import canny
from skimage import data
from matplotlib import cm
from astropy.stats import sigma_clipped_stats
import argparse
from scipy.optimize import fsolve
from astropy import wcs
from astroquery.irsa import Irsa
import astropy.units as u
from astropy import coordinates
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from astroquery.vizier import Vizier

PATH_TO_SCRIPT = os.path.dirname(os.path.realpath(__file__))
PATH_TO_PACKAGE = os.path.split(PATH_TO_SCRIPT)[0].split('IMP_NEW')[0]
sys.path.append(PATH_TO_PACKAGE + '/FindFluxes')

import rotate_psf
import ds9_contour
import mask_indiv
import imp_masking
import convert_segm_to_region
import imp_rebin

psf_image = '/home/amosenko/CurrentWork/PSF_Aniano/tar/Corrected_PSF_PACS_100.fits'
sigma_numb = 2
box_size = 5
FWHM = 8./3.


def hyperleda_galaxies(RA, DEC, Radius):
    viz = Vizier(keywords=["galaxies", "optical"])
    viz.ROW_LIMIT = -1

    coords_cen = coordinates.SkyCoord(RA,DEC,unit=('deg','deg'),frame='fk5')
    result = viz.query_region(coords_cen, radius=Radius*u.arcsec, catalog=["VII/237"])
    '''
    if result is None or len(result) == 0:

        ra_span *= 1.0 + 1e-5
        result = viz.query_region(center.to_astropy(), width=ra_span, height=dec_span, catalog=["VII/237"])
    '''
    table = result[0]
    #print table["logD25"][0]
    #print table["logR25"][0]
    #print table["PA"][0]
    return table



def show_complete(i,N):
  percent = 100. * float(i) / float(N)
  sys.stdout.write("\r%2d%%" % percent)
  sys.stdout.flush()

def prepare_psf(PosAng):
    xc = 120.
    yc = 120.

    rotate_psf.rotate_psf(psf_image, xc, yc, PosAng)
    # Upload the PSF image:
    hdulist_psf = pyfits.open('rot_psf.fits')
    data_psf = hdulist_psf[0].data
    Imax_psf = np.max(data_psf)
    (ycen_psf, xcen_psf) = np.where(data_psf==Imax_psf)

    #os.remove('rot_psf.fits')
    return data_psf,Imax_psf,ycen_psf, xcen_psf
  
def outer_contour(old_image_data, level, xc_psf, yc_psf, xc, yc, f_tmp, unmask_inner_stars=False, object_region_area=None, f_galaxies=None): 
  nx, ny =old_image_data.shape[1], old_image_data.shape[0]
  image_data = np.zeros(shape=(ny,nx))
  np.putmask(image_data, old_image_data>=level, 1.)

  contours = measure.find_contours(image_data, 0)

  Areas = []; STR = ''
  for n, contour_old in enumerate(contours):
	      contour = contour_old
	      x = contour[:, 1]
	      y = contour[:, 0]

	      XX = x - xc_psf

	      if x[0]>x[-1]:
		x = list(reversed(x))
		y = list(reversed(y))
	      else:
		x=list(x)
		y=list(y)

	      # Right top corner
	      if (x[0]>=nx-1 and y[-1]>=ny-1) or (x[-1]>=nx-1 and y[0]>=ny-1):
		x.append(float(nx-1))
		y.append(float(ny-1))

	      # Right bottom corner
	      if (x[0]>=nx-1 and y[-1]<=0) or (x[-1]>=nx-1 and y[0]<=0):
		x.append(float(nx-1))
		y.append(float(0))

	      # Left top corner
	      if (x[0]<=0 and y[-1]>=ny-1) or (x[-1]<=0 and y[0]>=ny-1):
		x.append(float(0))
		y.append(float(ny-1))

	      # Left bottom corner
	      if (x[0]<=0 and y[-1]<=0) or (x[-1]<=0 and y[0]<=0):
		x.append(float(0))
		y.append(float(0))

	      if unmask_inner_stars==False:
		  for k in range(len(x)):
		    if k==0:
		      f_tmp.write('polygon(%.1f,%.1f,' % (x[k]+1.-xc_psf+xc,y[k]+1.-yc_psf+yc) )
		    elif k>0 and k<len(x)-1:
		      f_tmp.write('%.1f,%.1f,' % (x[k]+1.-xc_psf+xc,y[k]+1.-yc_psf+yc) )
		    else:
		      f_tmp.write('%.1f,%.1f)\n' % (x[k]+1-xc_psf+xc,y[k]+1.-yc_psf+yc) )
	      else:
		    pol = []
		    for k in range(len(x)):
		      pol.append((int(float(x[k]+1.-xc_psf+xc)),int(float(y[k]+1.-yc_psf+yc))))
		    try:
		      polygon = Polygon(pol)
		      Areas.append(polygon.area)
		    except:
		      z=1
		    
		    for k in range(len(x)):  
		      if k==0:
			Str = 'polygon(%.1f,%.1f,' % (x[k]+1.-xc_psf+xc,y[k]+1.-yc_psf+yc)
		      elif k>0 and k<len(x)-1:
			Str = Str + '%.1f,%.1f,' % (x[k]+1.-xc_psf+xc,y[k]+1.-yc_psf+yc)
		      else:
			Str = Str + '%.1f,%.1f)\n' % (x[k]+1-xc_psf+xc,y[k]+1.-yc_psf+yc)
		    STR = STR + Str
  #print Areas
  if unmask_inner_stars==True and len(Areas)>=1:
    if max(Areas)>0.1 * object_region_area: 
      f_galaxies.write('point(%f,%f) # point=x\n' % (xc+1.,yc+1.) )
    else:
      f_tmp.write(STR)
  else:
      f_tmp.write(STR)

  
def pointInEllipse(x,y,xp,yp,d,D,angle):
    #tests if a point[xp,yp] is within
    #boundaries defined by the ellipse
    #of center[x,y], diameter d D, and tilted at angle
    angle = np.radians(angle)
    cosa=math.cos(angle)
    sina=math.sin(angle)
    dd=d/2*d/2
    DD=D/2*D/2

    a =math.pow(cosa*(xp-x)+sina*(yp-y),2)
    b =math.pow(sina*(xp-x)-cosa*(yp-y),2)
    ellipse=(a/dd)+(b/DD)

    if ellipse <= 1:
        return True
    else:
        return False    

def main(input_image, median, std, PA_PSF, output_clean_image='galaxy_clean.fits', output_mask='mask.fits',output_region_mask='mask.reg', RA=None, DEC=None, saturation_level = 1000., sextractor=False, object_region=None, add_cat_stars=False, unmasked_galaxies=None, unmask_inner_stars=False):
  f_tmp = open('test_mask.reg','w')
  f_tmp.write('%s\n' % ('image') )  

  # Determine pixel scale
  pix2sec, note = imp_rebin.resolution(input_image)
  pix2sec = float(pix2sec)
  
  if object_region!=None:
    object_region_area = object_region.area

  if sextractor==True and object_region!=None:
    x,y = object_region.exterior.xy
    for k in range(len(x)):
      if k==0:
	aa = 'polygon('
      if k>0 and k<len(x)-1:
	aa = aa + str(x[k])+','+str(y[k])+','
      if k==len(x)-1:
	aa =  aa + str(x[k])+','+str(y[k])+')'
    f_tmp.write(aa+' # color=blue\n')  

  data_psf, Imax_psf, ycen_psf, xcen_psf = prepare_psf(PA_PSF)
  stdev =  FWHM / ( 2.*np.sqrt(2.*np.log(2.)) )

  hdulist = pyfits.open(input_image)
  data = hdulist[0].data
  nx, ny =data.shape[1], data.shape[0]
  header = hdulist[0].header

  w = wcs.WCS(header)
  world = np.array([[RA,DEC]], np.float_)
  pixcrd = w.wcs_world2pix(world, 1)
  galaxies_coords = []
  galaxies_coords.append([pixcrd[0][0],pixcrd[0][1]])
  
  if unmasked_galaxies!=None:
    f_galaxies = open(unmasked_galaxies, 'w')
    f_galaxies.write('%s\n' % ('image') )
    #f_galaxies.write('point(%f,%f) # point=x\n' % (pixcrd[0][0]+1.,pixcrd[0][1]+1.) )




  #**************************************************************
  # 1. Mask extended objects catalogized in 2MASS XSC (fp_xsc):
  if object_region!=None:   
    world_cen = w.wcs_pix2world(np.array([[nx/2.,ny/2.]], np.float_), 1)
    coords_cen = coordinates.SkyCoord(world_cen[0][0],world_cen[0][1],unit=('deg','deg'),frame='fk5')
    
    #********************************************************************************
    # Query to search in the cone with the center at the center of the frame:
    Radius = math.sqrt(nx**2+ny**2)*pix2sec / 2.	# in arcsec
    table = Irsa.query_region(coords_cen, catalog='fp_xsc', spatial='Cone', radius= Radius * u.arcsec)
    
    # Add more from HyperLeda:
    #table_HL = hyperleda_galaxies(world_cen[0][0], world_cen[0][1], Radius)


    polygon = object_region
    
    # Find the nearest object to our galaxy (which should be our galaxy)
    dist = []
    for k in range(len(table)):
      dist.append(sqrt( (table['ra'][k]-RA)**2 + (table['dec'][k]-DEC)**2 ) * 3600.)       
    
    try:
      nearest_index = dist.index(min(dist))
    except:
      z=1
    
    outer_objects = []
    for k in range(len(table)):      
      pixcrd_ext = w.wcs_world2pix(np.array([[table['ra'][k],table['dec'][k]]], np.float_), 1)
      point_ext = Point(pixcrd_ext[0][0], pixcrd_ext[0][1])
      '''
      x,y = polygon.exterior.xy
      plt.plot(x,y,'r-')
      plt.plot([pixcrd_ext[0][0]],[pixcrd_ext[0][1]],'o')
      plt.show()
      '''
      if polygon.contains(point_ext):
	  # Check if this source belongs to the Object region and NOT our galaxy (galaxies outside the Object region will be masked by SExtractor):
	  sma_object = 2.5 * float(table['r_k20fe'][k]) 
	  smb_object = 1.5*float(sma_object * table['k_ba'][k])
	  PA_object = float(table['k_phi'][k]) + 90.
	  if np.isnan(sma_object)==False and np.isnan(smb_object)==False and np.isnan(PA_object)==False:
	    if unmasked_galaxies!=None:
	      #f_galaxies.write('point(%f,%f) # point=x\n' % (pixcrd_ext[0][0]+1.,pixcrd_ext[0][1]+1.) )
	      f_galaxies.write('ellipse(%1.1f,%1.1f,%1.1f,%1.1f,%1.1f) # color=red\n' % (int(pixcrd_ext[0][0])+1, int(pixcrd_ext[0][1])+1, sma_object, smb_object, PA_object))
	      if k!=nearest_index:
		galaxies_coords.append([pixcrd_ext[0][0],pixcrd_ext[0][1]])
	    else:
	      if k!=nearest_index:
		f_tmp.write('ellipse(%1.1f,%1.1f,%1.1f,%1.1f,%1.1f) # color=red\n' % (int(pixcrd_ext[0][0])+1, int(pixcrd_ext[0][1])+1, sma_object, smb_object, PA_object))
      else:
	  sma_object = 2.5 * float(table['r_k20fe'][k]) 
	  smb_object = 1.5*float(sma_object * table['k_ba'][k])
	  PA_object = float(table['k_phi'][k]) + 90.
	  if np.isnan(sma_object)==False and np.isnan(smb_object)==False and np.isnan(PA_object)==False:
	    outer_objects.append(k)
	    
    #********************************************************************************
    
    if add_cat_stars == True:
	#********************************************************************************    
	# 2. Add catalogized stars:
	table_stars = Irsa.query_region(coords_cen, catalog='allwise_p3as_psd', spatial='Cone', radius= Radius * u.arcsec)
	#print table_stars
	for k in range(len(table_stars)):      
	  pixcrd_star = w.wcs_world2pix(np.array([[table_stars['ra'][k],table_stars['dec'][k]]], np.float_), 1)
	  point_star = Point(pixcrd_star[0][0], pixcrd_star[0][1])
	  
	  if polygon.contains(point_star) and table_stars['ph_qual'][k][0]=='A' and int(table_stars['ext_flg'][k])<=1:
	    # If this source belongs to the Object region:
	    
	    f_tmp.write('ellipse(%1.1f,%1.1f,%1.1f,%1.1f,%1.1f) # color=red\n' % (int(pixcrd_star[0][0])+1, int(pixcrd_star[0][1])+1, 5., 5., 0.))    
	#********************************************************************************  

  
  #**************************************************************
  # 3. Detect stars as peaks
  threshold = median + (sigma_numb * std)
  tbl = find_peaks(data, threshold, box_size=box_size, subpixel=False, mask=None)
  tbl.sort('peak_value')
  tbl.reverse()


  NN = float(len(tbl))
  sat_coordinates = []; count_sat = 0
  for k in range(len(tbl)):
	xc = tbl['x_peak'][k]
	yc = tbl['y_peak'][k]
	
	mask_this = True
	for ii in range(len(galaxies_coords)):
	  if sqrt((galaxies_coords[ii][0]-xc)**2+(galaxies_coords[ii][1]-yc)**2)<=5:
	      # Don't mask the galaxy!
	      mask_this = False
	      break
	      
	if mask_this == False:
	  continue
	
	Imax = tbl['peak_value'][k]

	# Find background around the star:
	Rin = int(math.ceil(5.*FWHM))
	Rout = int(math.ceil(7.*FWHM))
	backgr = []
	X = []; Y = []; I = []
	for y in range(int(yc)-Rout,int(yc)+Rout,1):
	  for x in range(int(xc)-Rout,int(xc)+Rout,1):
	    try:
	      I.append(data[y,x])
	      X.append(x)
	      Y.append(y)
	      if (x-int(xc))**2+(y-int(yc))**2>Rin**2 and (x-int(xc))**2+(y-int(yc))**2<=Rout**2 and x>=0 and y>=0 and x<nx and y<ny:
		backgr.append(data[y,x])
	    except:
	      zz = 1
	Imax = Imax-np.median(backgr)
	psf = data_psf * Imax / Imax_psf
	
	point_star = Point(xc, yc)
	if unmask_inner_stars==True:
	  if polygon.contains(point_star):
	    outer_contour(psf, threshold, xcen_psf, ycen_psf, xc, yc, f_tmp, unmask_inner_stars, object_region_area, f_galaxies)      
	  else:
	    outer_contour(psf, threshold, xcen_psf, ycen_psf, xc, yc, f_tmp)
	else:
	  outer_contour(psf, threshold, xcen_psf, ycen_psf, xc, yc, f_tmp)
	

  #**************************************************************

  if RA!=None and DEC!=None:
      if sextractor==True:
	f_tmp.close()
	# 4. Apply SExtractor for additional masking except for those objects which have been already masked
	if os.path.exists('sex_tmp_mask_final.reg'):
	  os.remove('sex_tmp_mask_final.reg')
	
	
	mask_indiv.mask(input_image, 'test_mask.reg', factor=1, output_image='galaxy_clean_tmp.fits', output_mask='add_mask_tmp.fits', show_running=False)
	imp_masking.auto_mask('galaxy_clean_tmp.fits','sex_tmp_mask.reg',sextr_setup='wise_masking.sex', models=False)
	
	try:
	  number,x_image,y_image = loadtxt('field.cat', usecols=[0,1,2], unpack=True, skiprows = 12)
	  NUMBERS = []
	  for ind in outer_objects:
	    pixcrd_ext = w.wcs_world2pix(np.array([[table['ra'][ind],table['dec'][ind]]], np.float_), 1)
	    point_ext = Point(pixcrd_ext[0][0], pixcrd_ext[0][1])
	    sma_object = 2.5 * float(table['r_k20fe'][ind]) 
	    smb_object = 1.5*float(sma_object * table['k_ba'][ind])
	    PA_object = float(table['k_phi'][ind]) + 90.
	    
	    for kk in range(len(number)):    
	      res_pix = pointInEllipse(x_image[kk],y_image[kk],pixcrd_ext[0][0],pixcrd_ext[0][1],sma_object*2.,smb_object*2.,PA_object) 
	      if res_pix==True:
		NUMBERS.append(number[kk])
	except:
	  NUMBERS = []
	  
	if len(NUMBERS)>=1:
	  hdulist_segm = pyfits.open('segm.fits')
	  data_segm  = hdulist_segm [0].data
	  ny_segm,nx_segm = np.shape(data_segm)  
	  data_segm_new = np.zeros(shape=(ny_segm,nx_segm))
	  for number in NUMBERS:
	    np.putmask(data_segm_new, data_segm==number, number)
	  hdu = pyfits.PrimaryHDU(data_segm_new)
	  hdu.writeto('segm.fits', clobber=True)
	  convert_segm_to_region.main('segm.fits', 'sex_tmp_mask_final.reg', scale=1.5)


	# Merge masks:
	with open('tmp1.reg', 'w') as outfile:
		outfile.write('image\n')
		with open('test_mask.reg') as infile:
		    Lines = infile.readlines()
		    for Line in Lines:
		      if "blue" not in Line and 'image' not in Line:
			outfile.write(Line)


	convert_segm_to_region.scale_region('tmp1.reg', 'test_mask.reg', 1.6)

	if os.path.exists('sex_tmp_mask_final.reg'):
	    filenames = ['sex_tmp_mask_final.reg', 'test_mask.reg']
	    with open('tmp1.reg', 'w') as outfile:
		outfile.write('image\n')
		for fname in filenames:
		    with open(fname) as infile:
			Lines = infile.readlines()
			for Line in Lines:
			  if "blue" not in Line and 'image' not in Line:
			    outfile.write(Line)
	    os.rename('tmp1.reg','final_mask.reg')
	else:
	  os.rename('test_mask.reg','final_mask.reg')
	  
	mask_indiv.mask(input_image, 'final_mask.reg', factor=1, output_image=output_clean_image, output_mask=output_mask, show_running=False)
	for file in ['field.cat','segm.fits','sex_tmp_mask_final.reg','sex_tmp_mask.reg','test_mask.reg']:
	      if os.path.exists(file): 
		os.remove(file)

      else:
	f_tmp.close()

	os.rename('test_mask.reg', 'tmp1.reg')
	convert_segm_to_region.scale_region('tmp1.reg', 'final_mask.reg', 1.6)
	os.remove('tmp1.reg')
	mask_indiv.mask(input_image, 'final_mask.reg', factor=1, output_image=output_clean_image, output_mask=output_mask, show_running=False)
  else:
    f_tmp.close()
    os.rename('test_mask.reg', 'final_mask.reg')
    mask_indiv.mask(input_image, 'final_mask.reg', factor=1, output_image=output_clean_image, output_mask=output_mask, show_running=False)    

  if unmasked_galaxies!=None:
    f_galaxies.close()