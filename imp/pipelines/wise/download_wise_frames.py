#!/usr/bin/python
# -*- coding:  cp1251 -*-

import os
import sys
import multiprocessing as mp
import numpy as np
import astropy
import astropy.io.votable
import montage_wrapper.commands
import shutil
import signal
import gc
import wget
import pdb
import time
import warnings
warnings.filterwarnings("ignore")

# Define a timeout handler
def Handler(signum, frame):
    raise Exception("Timout!")

# Define function to wget WISE tiles
def WISE_wget(tile_url, tile_filename):
    #print 'Acquiring '+tile_url
    if os.path.exists(tile_filename):
        os.remove(tile_filename)
    success = False
    while success==False:
        try:
            wget.download(tile_url, out=tile_filename)
            #print 'Successful acquisition of '+tile_url
            success = True
        except:
            #print 'Failure! Retrying acquistion of '+tile_url
            time.sleep(0.1)
            success = False

# Define function to Montage together contents of folder
def WISE_Montage(name, ra, dec, width, band, input_dir, out_dir):
    #print 'Montaging '+name+'_WISE_'+band
    if os.path.exists(input_dir+band+'/Montage_Temp'):
        shutil.rmtree(input_dir+band+'/Montage_Temp')
    location_string = str(ra)+' '+str(dec)
    os.makedirs(input_dir+band+'/'+'Montage_Temp')
    os.chdir(input_dir+band+'/'+'Montage_Temp')
    montage_wrapper.commands.mHdr(location_string, width, input_dir+band+'/'+str(name)+'_HDR', pix_size=(3.819444391411E-4*3600))
    montage_wrapper.commands.mExec('2MASS', band.lower(), raw_dir=input_dir+band+'/', level_only=False, corners=False, debug_level=None, output_image=out_dir+'/'+name+'_WISE_'+band+'.fits', region_header=input_dir+band+'/'+str(name)+'_HDR', workspace_dir=input_dir+band+'/'+'Montage_Temp')
    shutil.rmtree(input_dir+band+'/Montage_Temp')


# Define function to Montage together error maps
def WISE_Montage_Error(name, ra, dec, width, band, input_dir, out_dir):
    #print 'Montaging '+name+'_WISE_'+band

    # Create header for reprojection
    location_string = str(ra)+' '+str(dec)
    montage_wrapper.commands.mHdr(location_string, width, os.path.join( input_dir, str(name)+'_HDR' ), pix_size=1.375)

    # Loop over error maps
    unc_files = os.listdir(input_dir)
    unc_first = True
    for i in range(0, len(unc_files)):
        unc_file = unc_files[i]
        if '.fits' not in unc_file:
            continue

        # Reproject error map, and read in
        montage_wrapper.wrappers.reproject( os.path.join( input_dir, unc_file ), os.path.join( input_dir, unc_file.replace('.fits.gz','_reproj.fits') ), header=os.path.join( input_dir, str(name)+'_HDR' ), exact_size=True, cleanup=True, silent_cleanup=True)
        unc_image, unc_header = astropy.io.fits.getdata( os.path.join( input_dir, unc_file.replace('.fits.gz','_reproj.fits') ), header=True )

        # Create array to co-add into
        if unc_first==True:
            wgt_coadd = np.zeros(unc_image.shape)
            unc_first = False

        # Convert error maps to effective exposure time maps, and co-add
        wgt_image = unc_image**-2.0
        wgt_bad = np.where( np.isnan(wgt_image)==True )
        wgt_image[ wgt_bad ] = 0.0
        wgt_coadd += wgt_image

    # Convert final effective exposure time map back into error map, and save to file
    if unc_first==False:
        unc_coadd = wgt_coadd**-0.5
        astropy.io.fits.writeto( os.path.join( out_dir, name+'_WISE_'+band+'_Error.fits' ), unc_coadd, header=unc_header, clobber=True )







# Commence main task
def main(name, ra, dec, d25, int_dir='Int', unc_dir = 'Unc', crop=False):
    initial_directory = os.getcwd()
    
    # Define paths
    in_dir = initial_directory + '/' + 'Temporary_Files/'
    out_dir = initial_directory + '/' + int_dir + '/'
    
    # Create directories
    if not os.path.exists(in_dir):
      os.mkdir(in_dir)

    if not os.path.exists(out_dir):
      os.mkdir(out_dir)

    ####################################################################################
    # Intensity image:
    # State bands
    #bands = ['W1','W2','W3','W4']
    bands = ['W1']
    # Register signal function handler, for dealing with timeouts
    #signal.signal(signal.SIGALRM, Handler)

    # Record time taken
    #time_total = 0.0
    #source_counter = 0.0
    #source_total = name_list.shape[0]
    #time_source_list = []

    # Loop over each source
    if True:
        #time_start = time.time()

        #source_counter += 1.0
        if crop==False:
	  if d25<6.0:
	      width = 0.5
	  elif d25>=6.0:
	      width = 1.0
	else:
	    width = d25/60.	# now in deg!
	    
        # Create tile processing dirctories (deleting any prior), and set appropriate Python (ie, Montage) working directory
        tile_dir = in_dir+str(name)+'/'
        if os.path.exists(tile_dir):
            shutil.rmtree(tile_dir)
        os.makedirs(tile_dir)
        os.makedirs(tile_dir+'/Raw')
        os.chdir(tile_dir+'/Raw')

        # Perform query (removing pre-existing query file, if present)
        #print 'Querying IRSA server'
        query_success = False
        while not query_success:
            try:
                query_url = 'http://irsa.ipac.caltech.edu/ibe/sia/wise/allwise/p3am_cdd?POS='+str(ra)+','+str(dec)+'&SIZE='+str(width)+'&INTERSECT=OVERLAPS'
                query_filename = tile_dir+str(name)+'.vot'
                if os.path.exists(query_filename):
                    os.remove(query_filename)
                wget.download(query_url, out=query_filename)
                query_success = True
            except:
                query_success = False
	#print query_success
        # Read query result VOTable
        query_output = astropy.io.votable.parse_single_table(query_filename)
        query_table = query_output.array

        # Extract FITS file URLs, and their respective bands, from table
        query_urls = list( query_table['sia_url'] )
        query_fits_boolean = np.array([ '.fits' in url for url in query_urls ])
        query_urls = list( np.array(query_urls)[ np.where( query_fits_boolean==True ) ] )
        query_bands = list(query_table['sia_bp_id'])
	#query_urls = [query_urls[0]]
	#query_bands = [query_bands[0]]
	
	# Select w1:
	indixes = []
	for j in range(0, len(query_urls)):
	  if '-w1-int-' in query_urls[j]:
	    indixes.append(j)

        # Delete VOTable
        os.remove(tile_dir+str(name)+'.vot')

        # In parallel, download files to holding directory
        #dl_pool = mp.Pool(processes=1)
        #for j in range(0, len(query_urls)):
        for j in indixes:
            tile_url = query_urls[j]
            tile_filename = in_dir+str(name)+'/Raw/'+str(name)+'_'+str(j)+'_'+str(query_bands[j])+'.fits'
            #dl_pool.apply_async( WISE_wget, args=(tile_url, tile_filename,) )
            WISE_wget(tile_url, tile_filename)
        #dl_pool.close()
        #dl_pool.join()

        # Copy files to relevant folders
        for band in bands:
            if os.path.exists(tile_dir+band+'/'):
                shutil.rmtree(tile_dir+band+'/')
            os.makedirs(tile_dir+band+'/')
            raw_files = np.array(os.listdir(tile_dir+'Raw/'))
            for raw_file in raw_files:
                if band+'.fits' in raw_file:
                    shutil.move(tile_dir+'Raw/'+raw_file, tile_dir+band)
        shutil.rmtree(tile_dir+'Raw')

        # In parallel, Montage together each band's input tiles, whilst dealing with timeouts
        complete = False
        while not complete:
            try:
                signal.alarm(1800)
                #pool = mp.Pool(processes=1)
                for band in bands:
                    #pool.apply_async( WISE_Montage, args=(name, ra, dec, width, band, tile_dir, out_dir,) )
                    WISE_Montage(name, ra, dec, width, band, tile_dir, out_dir)
                #pool.close()
                #pool.join()
                complete = True
            except Exception, exception_msg:
                #gc.collect()
                for band in bands:
                    input_dir = tile_dir+band+'/'
                    if os.path.exists(input_dir+'/Montage_Temp'):
                        shutil.rmtree(input_dir+'/Montage_Temp')
                #print exception_msg

        # Clean memory
        shutil.rmtree(tile_dir)
        #gc.collect()
        #shutil.rmtree(in_dir)

    # Define paths
    in_dir = initial_directory + '/' + 'Temporary_Files/'
    out_dir = initial_directory + '/' + unc_dir + '/'
    
    # Create directories
    if not os.path.exists(in_dir):
      os.mkdir(in_dir)

    if not os.path.exists(out_dir):
      os.mkdir(out_dir)


    ####################################################################################
    # Error image:
    # State bands
    #bands = ['W1','W2','W3','W4']
    bands = ['W1']
    # Register signal function handler, for dealing with timeouts
    signal.signal(signal.SIGALRM, Handler)

    # Record time taken
    #time_total = 0.0
    #source_counter = 0.0
    #source_total = name_list.shape[0]
    #time_source_list = []

    # Loop over each source
    if True:
        #time_start = time.time()
        if crop==False:
	  if d25<6.0:
	      width = 0.5
	  elif d25>=6.0:
	      width = 1.0
	else:
	    width = d25/60.	# now in deg!
        #print 'Processing source '+name

        # Create tile processing dirctories (deleting any prior), and set appropriate Python (ie, Montage) working directory
        gal_dir = in_dir+str(name)+'/'
        if os.path.exists(gal_dir):
            shutil.rmtree(gal_dir)
        os.makedirs(gal_dir)
        os.makedirs(gal_dir+'/Raw')
        os.chdir(gal_dir+'/Raw')

        # Perform query (removing pre-existing query file, if present)
        #print 'Querying IRSA server'
        query_success = False
        while not query_success:
            try:
                query_url = 'http://irsa.ipac.caltech.edu/ibe/sia/wise/allwise/p3am_cdd?POS='+str(ra)+','+str(dec)+'&SIZE='+str(width)+'&INTERSECT=OVERLAPS'
                query_filename = gal_dir+str(name)+'.vot'
                if os.path.exists(query_filename):
                    os.remove(query_filename)
                wget.download(query_url, out=query_filename)
                query_success = True
            except:
                query_success = False

        # Read query result VOTable
        query_output = astropy.io.votable.parse_single_table(query_filename)
        query_table = query_output.array

        # Extract FITS file URLs, and their respective bands, from table
        query_urls = list( query_table['sia_url'] )
        query_fits_boolean = np.array([ '.fits' in url for url in query_urls ])
        query_urls = list( np.array(query_urls)[ np.where( query_fits_boolean==True ) ] )
        query_bands = list(query_table['sia_bp_id'])
	#query_urls = [query_urls[0]]
	#query_bands = [query_bands[0]]

        # Alter URLs so that they retrieve uncertainty maps instead
        query_urls = [ query_url.replace('-int-','-unc-').replace('.fits','.fits.gz') for query_url in query_urls ]

	# Select w1:
	indixes = []
	for j in range(0, len(query_urls)):
	  if '-w1-unc-' in query_urls[j]:
	    indixes.append(j)

        # Delete VOTable
        os.remove(gal_dir+str(name)+'.vot')

        # In parallel, download files to holding directory
        #dl_pool = mp.Pool(processes=1)
        #for j in range(0, len(query_urls)):
        for j in indixes:
            tile_url = query_urls[j]
            tile_filename = in_dir+str(name)+'/Raw/'+str(name)+'_'+str(j)+'_'+str(query_bands[j])+'.fits'
            #dl_pool.apply_async( WISE_wget, args=(tile_url, tile_filename,) )
            WISE_wget(tile_url, tile_filename)
        #dl_pool.close()
        #dl_pool.join()

        # Copy files to relevant folders
        for band in bands:
            if os.path.exists(gal_dir+band+'/'):
                shutil.rmtree(gal_dir+band+'/')
            os.makedirs(gal_dir+band+'/')
            raw_files = np.array(os.listdir(gal_dir+'Raw/'))
            for raw_file in raw_files:
                if band+'.fits' in raw_file:
                    shutil.move(gal_dir+'Raw/'+raw_file, gal_dir+band)
        shutil.rmtree(gal_dir+'Raw')

        # In parallel, Montage together each band's input tiles, whilst dealing with timeouts
        complete = False
        while not complete:
            try:
                signal.alarm(1800)
                #pool = mp.Pool(processes=1)
                for band in bands:
                    #pool.apply_async( WISE_Montage_Error, args=(name, ra, dec, width, band, os.path.join(gal_dir,band), out_dir,) )
                    WISE_Montage_Error(name, ra, dec, width, band, os.path.join(gal_dir,band), out_dir)
                #pool.close()
                #pool.join()
                complete = True
            except Exception, exception_msg:
                #gc.collect()
                for band in bands:
                    input_dir = gal_dir+band+'/'
                    if os.path.exists(input_dir+'/Montage_Temp'):
                        shutil.rmtree(input_dir+'/Montage_Temp')
                #print exception_msg

        # Clean memory
        shutil.rmtree( os.path.join( in_dir, name ) )
        #gc.collect()
        
        #shutil.rmtree(in_dir)
    gc.collect()
    input_int_image = initial_directory + '/' + int_dir + '/'+name+'_WISE_W1.fits'
    input_unc_image = initial_directory + '/' + unc_dir + '/'+name+'_WISE_W1_Error.fits'
    return input_int_image,input_unc_image
      
#input_int_image,input_unc_image = main('ESO116-012', 48.2697, -57.35714, 3.6307800693276415)
#print input_int_image,input_unc_image