#!/usr/bin/python

# Import standard modules
import os
import os.path
import math
import numpy as np
#from scipy import ndimage
import shutil

# Modules for plotting
#import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm

# Import astronomic modules
#import aplpy
#import pyregion
import astropy.io.fits as pyfits
from astropy import wcs
import glob
import argparse

# Disable astropy logging except for warnings and errors
from astropy import log
log.setLevel("WARNING")

import warnings
warnings.filterwarnings("ignore")


#*** Colour fonts ***
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''




## This function rebins the currently selected frame(s) based on a certain reference FITS file
def rebin(reference_image,input_image,output_image, preserve_bad_pixels=True):
        # Import the rebinning function
        from hcongrid import hcongrid

        # Open the HDU list for the reference FITS file
        hdulist = pyfits.open(reference_image)

        # Get the primary image
        hdu = hdulist[0]

        referenceheader = hdu.header

        referenceheader["NAXIS"] = 2
        referenceheader.pop("NAXIS3", None)
	
        # Obtain the coordinate system from the reference header
        coordinates = wcs.WCS(referenceheader)

	hdulist_in = pyfits.open(input_image)
	data_in = hdulist_in[0].data
	  

	header_in = hdulist_in[0].header
	if 'NAXIS3' in header_in:
	  # For CUBE images
	  del header_in['NAXIS3']
	  del header_in['NAXIS']
	  hdu_tmp = pyfits.PrimaryHDU(data_in[0],header_in)
	  hdu_tmp.writeto('tmp.fits',clobber=True)
	  hdulist_in = pyfits.open('tmp.fits')
	  data_in = hdulist_in[0].data
	  header_in = hdulist_in[0].header
	  os.remove('tmp.fits')

	#print header_in
        # Do the rebinning based on the header of the reference image

	data_out = hcongrid(data_in, header_in, referenceheader, preserve_bad_pixels=preserve_bad_pixels)

	  
        # Use the coordinate system of the reference image to create the new header for this image
        header_out = coordinates.to_header()

	hdu_out = pyfits.PrimaryHDU(data_out,header_out)
	hdu_out.writeto(output_image,clobber=True)



def resolution(input_image):
    from astropy.io import fits
    hdulist = fits.open(input_image)#, ignore_missing_end=True)
    header = hdulist[0].header

    pixelscale = 1.
    note = ' '

    if 'PIXSCALE' in header:

        pixelscale = header['PIXSCALE']

    elif 'SECPIX' in header:

        pixelscale = header['SECPIX']

    elif 'PFOV' in header:

        pixelscale = header['PFOV']

    elif 'CD1_1' in header and 'CD1_2' in header:

        pixelscale = math.sqrt(header['CD1_1']**2 + header['CD1_2']**2 ) * 3600.0

    elif 'CD1_1' in header:

        pixelscale = abs(header['CD1_1']) * 3600.0

    elif 'CDELT1' in header:

        pixelscale = abs(header['CDELT1']) * 3600.0

    else:

        print "Could not determine the pixel scale from the image header. Set to 1 pix/arcsec."
        note = '*'

    # Return the pixel scale (in arcseconds)
    hdulist.close()
    return str(pixelscale),note



def main(arr = [], inp_dir = 'raw/', mode = 'a', outp_dir = './rebin', best_res_inp = None):
  print '\nWorking with the data:'
  print '\t\t\t',bcolors.OKBLUE+ 'pixel scale (arcsec)' + bcolors.ENDC
  
  ## Define the list of images to be rebinned and sort this list
  if arr == []:
    arr = glob.glob('%s*.fits' % (inp_dir))
    arr = sorted(arr)
  else:
    arr = sorted(arr)
  #print arr
  #exit()
  ## Select the image with the best resolution:
  k = 0
  PIX2SEC = []

  for ar in arr:
    k = k + 1
    pix2sec,note = resolution(ar)
    PIX2SEC.append(float(pix2sec))
    print '\t',k,'\t',ar.split('/')[1]
    print '\t\t\t', bcolors.OKBLUE+ str(pix2sec) + bcolors.ENDC,str(note)

  best_res = float(min(PIX2SEC))
  best_res_frame = PIX2SEC.index(best_res)
  if mode=='sa':
    best_res_frame = int(input('Please choose the reference (best resoluted) image (%i): ' % (best_res_frame+1)) - 1)
  elif mode=='a':
    print 'Image with the best resolution ' +str(PIX2SEC[best_res_frame])+ ' is:',bcolors.OKGREEN+arr[best_res_frame]+ bcolors.ENDC    
  best_res = PIX2SEC[best_res_frame]
  
  if mode=='m' and best_res_inp!=None:
    best_res = best_res_inp


  ## Print rescaling factor for each frame:
  res_factor = []
  print '\n\t\t\t',bcolors.OKBLUE+ 'rescaling factor' + bcolors.ENDC
  
  for k in range(len(arr)):
      res_factor.append(best_res/PIX2SEC[k])
      print '\t',k+1,'\t',arr[k].split('/')[1]
      print '\t\t\t', bcolors.OKBLUE+ str(best_res/PIX2SEC[k]) + bcolors.ENDC,str(note)

  ## Create the directory where to put the output files:  
  if not os.path.exists(outp_dir):
    os.makedirs(outp_dir)
      
  ## Rebin each frame:    
  for k in range(len(arr)):
    filename = arr[k].split('/')[1]
    image_name = filename.split('.')[0] # name of the image without .fits
    print '\n%s %i[%i]' % (image_name,k+1,len(arr))
    survey = image_name.split('_')[0]
    
    # 1. Check if there are NaN values and fix them (set 0)
    try:
      shutil.copy(arr[k],'no_nan.fits') 
      hdulist3 = pyfits.open('no_nan.fits', do_not_scale_image_data=True, mode='update')
      img3 = hdulist3[0].data
      img3[np.isnan(img3)]=0
      hdulist3.flush()
    except:
      # Try another fits layer: UKIDSS
      hdulist3 = pyfits.open(arr[k])
      img3 = hdulist3[1].data
      head = hdulist3[1].header
      img3[np.isnan(img3)]=0
      hdu = pyfits.PrimaryHDU(img3,head)
      hdu.writeto('no_nan.fits',clobber=True)      
      
    if len(arr)==1:
      # Do nothing
      os.rename('no_nan.fits',image_name+"_r.fits")      
    else:
	# 2. Change resolution of the image in correspondance with the reference image
	if best_res_inp==None:
	  if arr[best_res_frame]!=arr[k]:
	    rebin(arr[best_res_frame],'no_nan.fits',image_name+"_r.fits")
	  else:
	    shutil.copy('no_nan.fits',image_name+"_r.fits")
	else:
	  import rebin_image
	  if res_factor[k]!=1.:
	    rebin_image.downsample('no_nan.fits', res_factor[k], image_name+"_r.fits")
	  else:
	    shutil.copy('no_nan.fits',image_name+"_r.fits")
	os.remove('no_nan.fits')

    # 3. Add pixel scale information to the header:
    hdulist = pyfits.open(image_name+"_r.fits", do_not_scale_image_data=True, mode='update')
    prihdr = hdulist[0].header
    prihdr.append(('PIXSCALE_OLD', PIX2SEC[k]),end=True)
    prihdr.append(('PIXSCALE_NEW', best_res),end=True)
    prihdr.append(('FACTOR_M0', res_factor[k]),end=True)  
    hdulist.flush()
      
    shutil.move(image_name+"_r.fits","./rebin/"+image_name+"_r.fits")

  '''
  #4. Create input file for PTS
  list_gal = os.listdir("./rebin")
  f = open("./rebin/files.dat","w")
  for k in range(len(list_gal)):
    if list_gal[k].split('.')[0]!='files':
      print >>f, list_gal[k].split('.')[0]
  f.close()
  '''
  print 'Done!'
  

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Rebinning")
    parser.add_argument("reference_image", help="Input reference image")
    parser.add_argument("input_image", help="Input image")    
    parser.add_argument("output_image", help="Output rebinned image")  
    args = parser.parse_args()

    reference_image = args.reference_image
    input_image = args.input_image
    output_image = args.output_image
    
    rebin(reference_image,input_image,output_image, preserve_bad_pixels=True)
  


#rebin('UGC10043_B_r_sub_rot_ext.fits','SDSS_i.fits','UGC10043_i_r_sub_rot_ext.fits')
#rebin('UGC10043_B_r_sub_rot_ext.fits','UGC10043.phot.1_nonan.fits','UGC10043_I1_r_sub_rot_ext.fits')
#mGetHdr UGC10043_B_r_sub_rot_ext.fits templ.hdr
#mProject K_final_crop_clean.fits K.fits templ.hdr



