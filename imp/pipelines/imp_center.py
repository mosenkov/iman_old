#!/usr/bin/env python
# -*- coding: utf8 -*-
# *****************************************************************
# **       PTS -- Python Toolkit for working with SKIRT          **
# **       © Astronomical Observatory, Ghent University          **
# *****************************************************************

## \package do.fitskirt_center
# IMPORTANT: should be run after fitskirt_rotate
#
# This routine reads in a files list and one coordinate set and two lower bounds.
# It cuts out the frames respecting the bounds and placing the coordinate in the center.
# Additionally, a rebinningfactor can be chosen.
#
# REQUIREMENTS: 
# - the files list should be free of ".fits" extension
# - the x-coordinate of the center
# - the y-coordinate of the center
# - the x-coordinate of the lower bound on the x-coordinate (left) 
# - the y-coordinate of the lower bound on the y-coordinate (down) 
#
# OPTIONAL:
# - integer n used to rebin the frame by combining (n,n) to (1,1)
#
# EXAMPLE: >pts fitskirt_center files.dat 1007 992 612 920 2
#
# This centers the frame from 612 to 1402 in x (so 1007 is the center)
# and 920 to 1064 in y (so 992 is the center) and rebins by 2,2
#
# -----------------------------------------------------------------

# Import the necessary modules
import pyfits
import numpy as np
import math
import itertools
import matplotlib.pyplot as plt
from scipy import ndimage
from numpy import *
import sys
import subprocess
import shutil
import os
import glob
from astropy.stats import sigma_clipped_stats

try:
  import astropy.io.fits as pyfits
  from astropy import wcs
except:
  warnings.warn("Astropy is not installed! No WCS has been added to the header!")

import imp_setup

PATH_TO_SCRIPT = os.path.dirname(os.path.realpath(__file__))
PATH_TO_PACKAGE = os.path.split(PATH_TO_SCRIPT)[0].split('IMP_NEW')[0]
sys.path.append(PATH_TO_PACKAGE + '/Warps')

import Find_pos_angle
import imp_rotate


# Colors to highlight the output text
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''




def center_manual(fileToShow):
    method = 'box'
    if method == 'box':
	if not os.path.exists('./reg/cen.reg'):  
	  open("cen.reg", "a").close()
	else:
	  shutil.copy('./reg/cen.reg','cen.reg')
	p = subprocess.Popen(["ds9",fileToShow,"-scale","histequ","-cmap","Cool","-regions","load","cen.reg","-contour","limits"])
	p.wait()
	f = open("cen.reg", "r") 
	lines = f.readlines()
	if method=='box':
	    for line in lines:
	      if 'box(' in line:	
		box = line.split(',')
		xc_box = float(box[0].split('(')[1])
		yc_box = float(box[1])    
		a_box = float(box[2])/2.
		b_box = float(box[3].split(')')[0])/2.
		x_l = xc_box - a_box
		y_l = yc_box - b_box
		x_r = xc_box + a_box
		y_r = yc_box + b_box
		break
    f.close()
    if os.path.exists('cen.reg'):
      shutil.move('cen.reg','./reg/cen.reg')
    return x_l,y_l,x_r,y_r


def crop(inputname, outputname,x_l,y_l,x_r,y_r):
    # x_l,y_l,x_r,y_r - pixel coordinates in DS9 format (start from 0.5, 0.5)
    hdulist = pyfits.open(inputname)
    referenceheader = pyfits.getheader(inputname, 0)
    #print referenceheader

    if referenceheader['CTYPE2'] == 'DEC---TAN':
      referenceheader['CTYPE2'] = 'DEC--TAN'
    '''
    if referenceheader['CTYPE1'] == 'RA---TAN':
      referenceheader['CTYPE1'] = 'RA--TAN'
    '''
    #exit()
      #del.referenceheader['DEC---TAN']

    if 'COMMENT' in referenceheader:
	del referenceheader['COMMENT']
    if 'HISTORY' in referenceheader:
	del referenceheader['HISTORY']
    if '' in referenceheader:
	del referenceheader['']  

    w = wcs.WCS(referenceheader)
    xcen = (x_l+x_r)/2.
    ycen = (y_l+y_r)/2.
    
    pixcrd = np.array([[xcen, ycen]], np.float_)
    world = w.wcs_pix2world(pixcrd, 1)

    xcen_w,ycen_w = world[0,0],world[0,1]

    inframe = hdulist[0].data
    ny,nx = np.shape(inframe)
    
    ref_pixel = inframe[int(math.floor(ycen-0.5)),int(math.floor(xcen-0.5))]
    inframe[int(math.floor(ycen-0.5)),int(math.floor(xcen-0.5))] = 999999
    
    if int(math.floor(y_l-0.5))<0:
	ymin = 0
	print "ymin is set to 0"
    else:
	ymin = int(math.floor(y_l-0.5))
    if int(math.floor(x_l-0.5))<0:
	xmin = 0
	print "xmin is set to 0"
    else:
	xmin = int(math.floor(x_l-0.5))
    if int(math.floor(y_r-0.5))>=ny:
	ymax = ny-1    
	print "ymax is set to dim_y"
    else:
	ymax = int(math.floor(y_r-0.5))
    if int(math.floor(x_r-0.5))>=nx:
	xmax = nx-1
	print "xmax is set to dim_x"
    else:
	xmax = int(math.floor(x_r-0.5))



    try:
      outframe = inframe[ymin:ymax+1,xmin:xmax+1]
    except:
      print bcolors.FAIL+'Wrong range to crop the image! Exiting ...' + bcolors.ENDC
      exit()

    ref_coords = np.where(outframe == 999999)

    ycen_new = int(ref_coords[0][0])
    xcen_new = int(ref_coords[1][0])
    #print ycen_new, xcen_new

    outframe[ycen_new,xcen_new] = ref_pixel
    referenceheader['CRPIX1'] = xcen_new+1.0
    referenceheader['CRPIX2'] = ycen_new+1.0
    referenceheader['CRVAL1'] = xcen_w
    referenceheader['CRVAL2'] = ycen_w
    #print xcen_w, ycen_w

    hdu = pyfits.PrimaryHDU(outframe,referenceheader)
    hdu.writeto(outputname,clobber=True)
    return xcen_new,ycen_new
    
    
def main(mode='m', inp_dir = 'rot/', outp_dir="./ext", ref_image=None, x_obj=None, y_obj=None, coords='world', m0=0., pix2sec=1):
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "           GALAXY CENTERING 2016         " + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    m0 = float(m0)
    pix2sec = float(pix2sec)
    
    if not os.path.exists(outp_dir):
      os.makedirs(outp_dir)
    '''
    if os.path.exists("./rot"):
      arr = glob.glob('rot/*.fits')
    '''
    arr = glob.glob('%s*.fits' % (inp_dir))
    if ref_image == None:
	ref_image = arr[0]

    if mode == 'm':
      for i in range(len(arr)):
	if ref_image in arr[i]:
	  x_l,y_l,x_r,y_r = center_manual(arr[i])
	  break
    else:
      for i in range(len(arr)):
	if ref_image in arr[i]: 
	  if coords=='world':
	    from astropy.wcs import WCS
	    w = WCS(ref_image)
	    [[xc,yc]] = w.wcs_world2pix([[x_obj,y_obj]], 1)
	  
	  hdulist_ima = pyfits.open(ref_image)
	  data_image = hdulist_ima[0].data

	  header = hdulist_ima[0].header
	  if 'Sky_std' in header:
		std_backgr = float(header['Sky_std'])
	  else:
		I_mean, I_median, std_backgr = sigma_clipped_stats(data_image, sigma=3.0, iters=5)

	  inner_level = m0 - 2.5*log10(imp_setup.inner_level * std_backgr) + 5.*log10(pix2sec)
	  outer_level = m0 - 2.5*log10(imp_setup.outer_level * std_backgr) + 5.*log10(pix2sec)

	  PA,xcc,ycc,smaa,smbb = Find_pos_angle.main(ref_image,m0,pix2sec,xc,yc,inner_level,outer_level)
	  x_l = xcc - smaa*imp_setup.exend_factor
	  y_l = ycc - smbb*imp_setup.exend_factor
	  x_r = xcc + smaa*imp_setup.exend_factor
	  y_r = ycc + smbb*imp_setup.exend_factor
	  fout = open("cen.reg", "w")
	  fout.truncate(0)
	  fout.write("# Region file format: DS9 version 4.1\n")
	  fout.write('global color=green dashlist=8 3 width=1 font="helvetica 10 ')
	  fout.write('normal roman" select=1 highlite=1 dash=0 fixed=0 ')
	  fout.write('edit=1 move=1 delete=1 include=1 source=1\n')
	  fout.write('image\n')
	  fout.write("box(%1.1f,%1.1f,%1.1f,%1.1f)\n" % (xcc,ycc, 2.*smaa*imp_setup.exend_factor, 2.*smbb*imp_setup.exend_factor))
	  fout.close()     
	  if mode=='sa':
	    p = subprocess.Popen(["ds9",ref_image,"-scale","histequ","-cmap","Cool","-regions","load","cen.reg","-contour","limits"])
	    p.wait()  
	    f = open("cen.reg", "r") 
	    lines = f.readlines()
	    for line in lines:
		if 'box(' in line:	
		    box = line.split(',')
		    xc_box = float(box[0].split('(')[1])
		    yc_box = float(box[1])    
		    a_box = float(box[2])/2.
		    b_box = float(box[3].split(')')[0])/2.
		    x_l = xc_box - a_box
		    y_l = yc_box - b_box
		    x_r = xc_box + a_box
		    y_r = yc_box + b_box
		    break
	    f.close()
	  if os.path.exists('cen.reg'):
	    shutil.move('cen.reg','./reg/cen.reg')    
	  break
	
    for i in range(len(arr)):
		inputname = arr[i]
		outputname = outp_dir+'/'+arr[i].split('/')[-1].split('.fits')[0]+"_ext.fits"
		xCenRot, yCenRot = crop(inputname, outputname,x_l,y_l,x_r,y_r)
		imp_rotate.add_header(['Sky_std','Sky_med','PA'],inputname,outputname)
    print bcolors.OKGREEN+ 'Coordinates of the new center are' + bcolors.ENDC, xCenRot, yCenRot 
    print 'Done!'
    return xCenRot, yCenRot     

#crop('H_final_rot.fits', 'H_final_rot_ext.fits',552,684,1285,998)
      
#main('m', outp_dir="./ext", ref_image=None, x_obj=181.76309, y_obj=43.06582, coords='world', m0=29.38, pix2sec=0.834)
'''
if __name__ == '__main__':
  inputname = sys.argv[1]
  outputname = sys.argv[2]
  x_l = float(sys.argv[3])
  y_l = float(sys.argv[4])
  x_r = float(sys.argv[5])
  y_r = float(sys.argv[6])
  crop(inputname, outputname,x_l,y_l,x_r,y_r)
'''