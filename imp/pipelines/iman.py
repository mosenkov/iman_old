#!/usr/bin/python
# -*- coding:  cp1251 -*-

# Import standard modules
import sys
import math
import numpy as np
from scipy import stats
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import matplotlib.path as path
from matplotlib.ticker import NullFormatter
from numpy import *
from pylab import *
import os
import shutil
import subprocess
import random
import re
import glob

import pyfits
import pickle
import time 

start_time = time.time()



PATH_TO_SCRIPT = os.path.dirname(os.path.realpath(__file__))
PATH_TO_PACKAGE = os.path.split(PATH_TO_SCRIPT)[0]
sys.path.append(PATH_TO_SCRIPT+'/FindFluxes')
sys.path.append(PATH_TO_SCRIPT+'/Decomposition')
sys.path.append(PATH_TO_SCRIPT+'/Plot')

def read_dec_dict(compiled_model, PARS, pix2sec, m0, D, Redsh, Aext, Col, Col_val):
  f = open('dec_results.dat','w')
  for comp in ['First_Disk','Second_Disk','Bulge']:
    print >>f, '\n\nComponent: %s' % (comp)
    for key, value in PARS.iteritems() :
	if ('_hd_' in str(key) or '_z0_' in str(key) or '_reb_' in str(key)) and np.isnan(value)==False and comp in str(key):
	  value_arcsec = value * pix2sec # in arcsec
	  value_kpc = 4.84*value_arcsec*D/1000.
	  print >>f, '\t%s:\t%.2f pix, %.2f arcsec, %.2f kpc' % (key,value,value_arcsec,value_kpc)
	if ('_m0d_' in str(key) or '_meb_' in str(key)) and np.isnan(value)==False and comp in str(key):
	  print >>f, '\t%s:\t%.2f mag/arcsec^2' % (key,value)
	if ('Bulge_n_2d' in str(key) or 'Bulge_q_2d' in str(key)) and np.isnan(value)==False and comp in str(key):
	  print >>f, '\t%s:\t%.2f' % (key,value)
	if comp=='Bulge' and 'D/T' in str(key):
	  print >>f, '\n%s:\t%.2f' % (key,value)

  hdulist = pyfits.open(compiled_model)
  ref_data = hdulist[0].data
  model_data = hdulist[1].data
  Flux_ref = np.sum(ref_data)
  Flux_model = np.sum(model_data)
  print >>f, 'Total mag (reference image and model): %.2f mag, %.2f mag' % (m0-2.5*log10(Flux_ref),m0-2.5*log10(Flux_model))
  f.close()


def write_report(r, input_file):
    for k, v in r.items():
        line = '{}, {}'.format(k, v) 
        print >>input_file, line   

def move_files(files_after,directory=None):
  for file_after in files_after:
    if float(os.path.getmtime(os.path.join('./', file_after)))>start_time:
      if directory==None:
	os.remove(file_after)
      else:
	shutil.move(file_after,directory + '/' + file_after)



warnings.filterwarnings("ignore")

if len(sys.argv)>1:
  if sys.argv[1]!='-use_basic':
    work_dir = sys.argv[1]
  else:
    work_dir = 'mask_non_norm'
else:
  work_dir = 'mask_non_norm'

use_basic = False 
 
for k in range( len(sys.argv) ):
  if sys.argv[k]=='-use_basic':
    use_basic = True

if not os.path.exists(work_dir):
  print 'The input directory does not exist. Exiting...'
  exit()

print '\n\n***********************************'
print '*************** IMAN ***************'
print '\nThe working directory with the imaging is: %s\n' % (work_dir)
print '\t0. Input basic information.'
print '\t1. Find total flux.'
print '\t2. Do preliminary decomposition.'
print '\t3. Do full decomposition.'
print '\t4. Do disc decomposition.'
print '\t5. Plot isophote map.'
print '\t6. IRAF/ellipse task.'
print '\t9. Create single pdf file with pictures.'
print '\t10. Obtain results as a txt and pdf file.\n'

action = str(1)
action = str(raw_input('Please choose the action (%s): ' % (action))) or action
action = int(action)

# Create new folder if needed where all the results will be placed
if not os.path.exists("./analysis"):
  os.makedirs("./analysis")  

# Remember the files in the directory before running script:
#files_before = glob.glob("*.*")

# Output all the images and choose one
print '\n\tIMAGES:'
arr = glob.glob(work_dir+'/*.fits')
k = 0
for ar in arr:
      k = k + 1
      print '\t',k,'\t',ar.split('/')[1]

N_beg = str(1)
N_beg = str(raw_input('Please choose the image to work with (%s): ' % (N_beg))) or N_beg
input_image = arr[int(N_beg)-1]
mask_image = input_image.split('.fits')[0] + '_mask.fits'

print 'The image is: %s' % (input_image)
print 'The mask-image is: %s' % (mask_image)

gal_dir = input_image.split('/')[-1].split('.fits')[0]



basic_file = "./analysis/" + input_image.split('/')[-1].split('.fits')[0] + "_BASIC.p"
ds9_file = "./analysis/" + input_image.split('/')[-1].split('.fits')[0] + "_DS9.p"
bps_file = "./analysis/" + input_image.split('/')[-1].split('.fits')[0] + "_BPS.p"
dec_file = "./analysis/" + input_image.split('/')[-1].split('.fits')[0] + "_DEC.p"
disc_dec_file = "./analysis/" + input_image.split('/')[-1].split('.fits')[0] + "_DISC_DEC.p"

if action==0:
  hdulist = pyfits.open(input_image)
  inframe = hdulist[0].data
  prihdr = hdulist[0].header
  nx, ny =inframe.shape[1], inframe.shape[0]  
  
  
  if not os.path.exists(basic_file): 
      BASIC_PARS = {}
      BASIC_PARS['mag_level'] = [float(nan)]
      BASIC_PARS['m0'] = [float(nan)]
      BASIC_PARS['pix2sec'] = [float(nan)]
      BASIC_PARS['FWHM'] = [float(nan)]
      BASIC_PARS['max_disc_radius']= [float(nan)]
      BASIC_PARS['min_disc_radius']= [float(nan)]
      BASIC_PARS['max_disc_z']= [float(nan)]
      BASIC_PARS['min_disc_z']= [float(nan)]
      BASIC_PARS['xc']= [float(nan)]
      BASIC_PARS['yc']= [float(nan)]
      BASIC_PARS['psf_image']= ['none']
      BASIC_PARS['D']= [float(nan)]
      BASIC_PARS['Redsh']= [float(nan)]
      BASIC_PARS['Aext']= [float(nan)]
      BASIC_PARS['Col']= [float(nan)]
      BASIC_PARS['Col_val']= [float(nan)]
	    
      # Input basic information for this image
      mag_level = 25.5
      m0 = 22.5
      pix2sec = 1.0
      FWHM = 1.0
      max_disc_radius = nx/2.
      min_disc_radius = 0.
      max_disc_z = ny/2.
      min_disc_z = 0.
      xc = nx/2.
      yc = ny/2.
      psf_image = 'none'
      D = float(nan)
      Redsh = float(nan)
      Aext = float(nan)
      Col = float(nan)
      Col_val = float(nan)
      
  else:
      BASIC_PARS = pickle.load( open( basic_file, "rb" ) )
      mag_level = BASIC_PARS['mag_level'][0]
      m0 = BASIC_PARS['m0'][0]
      pix2sec = BASIC_PARS['pix2sec'][0]
      FWHM = BASIC_PARS['FWHM'][0]
      max_disc_radius = BASIC_PARS['max_disc_radius'][0]
      min_disc_radius = BASIC_PARS['min_disc_radius'][0]
      max_disc_z = BASIC_PARS['max_disc_z'][0]
      min_disc_z = BASIC_PARS['min_disc_z'][0]
      xc = BASIC_PARS['xc'][0]
      yc = BASIC_PARS['yc'][0]
      psf_image = BASIC_PARS['psf_image'][0]
      D = BASIC_PARS['D'][0]
      Redsh = BASIC_PARS['Redsh'][0]
      Aext = BASIC_PARS['Aext'][0]
      Col = BASIC_PARS['Col'][0]
      Col_val = BASIC_PARS['Col_val'][0]


  
  mag_level = str(raw_input('Please input outer isophote level in [mag/arcsec^2] (%s): ' % (str(mag_level)))) or str(mag_level)
  m0 = str(raw_input('Please input Zero Point in [mag] (%s): ' % (str(m0)))) or str(m0)
  pix2sec = str(raw_input('Please input scale [arcsec/pix] (%s): ' % (str(pix2sec)))) or str(pix2sec)
  FWHM = str(raw_input('Please input FWHM [pix] (%s): ' % (str(FWHM)))) or str(FWHM)

  xc = str(raw_input('Please input x-center of the galaxy [pix] (%s): ' % (str(xc)))) or str(xc)
  yc = str(raw_input('Please input y-center of the galaxy [pix] (%s): ' % (str(yc)))) or str(yc)  

  max_disc_radius = str(raw_input('Please input maximal disc radius (truncation) [pix] (%s): ' % (str(max_disc_radius)))) or str(max_disc_radius)
  min_disc_radius = str(raw_input('Please input minimal disc radius [pix] (%s): ' % (str(min_disc_radius)))) or str(min_disc_radius)
  
  max_disc_z = str(raw_input('Please input maximal Z-extent [pix] (%s): ' % (str(max_disc_z)))) or str(max_disc_z) 
  min_disc_z = str(raw_input('Please input minimal Z-extent [pix] (%s): ' % (str(min_disc_z)))) or str(min_disc_z)
  
  psf_image = str(raw_input('Please input PSF image [pix] (%s): ' % (psf_image))) or str(psf_image)

  D = str(raw_input('Please input distance to the object [Mpc] (%s): ' % (str(D)))) or str(D) 
  Redsh = str(raw_input('Please input redshift (%s): ' % (str(Redsh)))) or str(Redsh) 
  Aext = str(raw_input('Please input extinction in this band [mag] (%s): ' % (str(Aext)))) or str(Aext) 
  Col = str(raw_input('Please input colour for K-correction  (%s): ' % (str(Col)))) or str(Col) 
  Col_val = str(raw_input('Please input colour value for K-correction [mag] (%s): ' % (str(Col_val)))) or str(Col_val) 
  
  BASIC_PARS['xc'] = [float(xc)]
  BASIC_PARS['yc'] = [float(yc)]
  BASIC_PARS['mag_level'] = [float(mag_level)]
  BASIC_PARS['m0'] = [float(m0)]
  BASIC_PARS['pix2sec'] = [float(pix2sec)]
  BASIC_PARS['FWHM'] = [float(FWHM)]
  BASIC_PARS['max_disc_radius'] = [float(max_disc_radius)]
  BASIC_PARS['min_disc_radius'] = [float(min_disc_radius)]
  BASIC_PARS['max_disc_z'] = [float(max_disc_z)]
  BASIC_PARS['min_disc_z'] = [float(min_disc_z)]
  BASIC_PARS['psf_image'] = [psf_image]
  BASIC_PARS['D']= [float(D)]
  BASIC_PARS['Redsh']= [float(Redsh)]
  BASIC_PARS['Aext']= [float(Aext)]
  BASIC_PARS['Col']= [str(Col)]
  BASIC_PARS['Col_val']= [float(Col_val)]
  
  pickle.dump( BASIC_PARS, open( basic_file, "wb" ) )
  

elif action==1:
  if not os.path.exists("./analysis/"+gal_dir+"/ds9_contour"):
    os.makedirs("./analysis/"+gal_dir+"/ds9_contour")  

  import ds9_contour

  if not os.path.exists(basic_file):
      mag_level = 25.5
      m0 = 22.5
      pix2sec = 1.0
      FWHM = 1.0
      str_input = str(mag_level)+','+str(m0)+','+str(pix2sec)+','+str(FWHM)
      str_input = str(raw_input('Please input mag_level, Zero-point, scale and FWHM using comma (%s): ' % (str_input))) or str_input
      
      str_input = str_input.split(',')
      mag_level = float(str_input[0])
      m0 = float(str_input[1])
      pix2sec = float(str_input[2])
      FWHM = float(str_input[3])
      
  else:
      BASIC_PARS = pickle.load( open( basic_file, "rb" ) )
      mag_level = float(BASIC_PARS['mag_level'][0])
      m0 = float(BASIC_PARS['m0'][0])
      pix2sec = float(BASIC_PARS['pix2sec'][0])
      FWHM = float(BASIC_PARS['FWHM'][0])
  
  print input_image,mask_image,mag_level,m0,pix2sec,FWHM
  PARS_DS9,isophote_file,isophote_image,error = ds9_contour.main(input_image,mask_image,mag_level,m0,pix2sec,FWHM)
  pickle.dump( PARS_DS9, open( ds9_file, "wb" ) )
  files_after = glob.glob("*.*")
  move_files(files_after,directory="./analysis/"+gal_dir+"/ds9_contour")

elif action == 2:
  if not os.path.exists("./analysis/"+gal_dir+"/bps"):
    os.makedirs("./analysis/"+gal_dir+"/bps")  

  BASIC_PARS = pickle.load( open( basic_file, "rb" ) )
  mag_level = BASIC_PARS['mag_level'][0]
  m0 = BASIC_PARS['m0'][0]
  pix2sec = BASIC_PARS['pix2sec'][0]

  if use_basic==True:
    xc = BASIC_PARS['xc'][0]
    yc = BASIC_PARS['yc'][0]
    max_disc_radius = BASIC_PARS['max_disc_radius'][0]
    min_disc_radius = BASIC_PARS['min_disc_radius'][0]
    max_disc_z = BASIC_PARS['max_disc_z'][0]
    min_disc_z = BASIC_PARS['min_disc_z'][0]
    
  else:
    PARS_DS9 = pickle.load( open( ds9_file, "rb" ) )
    [xc,yc,max_disc_radius,max_disc_z,phi] = PARS_DS9['Isophote']
    min_disc_z = 0.
    min_disc_radius = 0.

  inter_mode = 'yes'
  inter_mode = str(raw_input('Do you want to work interactively? (YES/no): ')) or inter_mode
  if inter_mode=='yes':
    inter_mode=True
  else:
    inter_mode=False
    
  import bps_disc_com_regions
  PARS_BPS,file_1,file_2,file_3 = bps_disc_com_regions.main(input_image,mask_image,mag_level,int(max_disc_radius),int(max_disc_z),int(min_disc_z),
			    m0,pix2sec,xc=int(xc),zc=int(yc),inter_mode=inter_mode,min_disc_radius=int(min_disc_radius),Profile='no-summed') # 'no-summed'
  pickle.dump( PARS_BPS, open( bps_file, "wb" ) )
  files_after = glob.glob("*.*")
  move_files(files_after,directory="./analysis/"+gal_dir+"/bps")

elif action == 3:
  import decomp_thin_thick_new
  if not os.path.exists("./analysis/"+gal_dir+"/dec"):
    os.makedirs("./analysis/"+gal_dir+"/dec")  

  BASIC_PARS = pickle.load( open( basic_file, "rb" ) )
  mag_level = BASIC_PARS['mag_level'][0]
  m0 = BASIC_PARS['m0'][0]
  pix2sec = BASIC_PARS['pix2sec'][0]

  if use_basic==True:

    xc = BASIC_PARS['xc'][0]
    yc = BASIC_PARS['yc'][0]
    max_disc_radius = BASIC_PARS['max_disc_radius'][0]
    min_disc_radius = BASIC_PARS['min_disc_radius'][0]
    max_disc_z = BASIC_PARS['max_disc_z'][0]
    min_disc_z = BASIC_PARS['min_disc_z'][0]
    psf_image = BASIC_PARS['psf_image'][0]
    print xc,yc,min_disc_radius
    
  else:
    PARS_DS9 = pickle.load( open( ds9_file, "rb" ) )
    [xc,yc,max_disc_radius,max_disc_z,phi] = PARS_DS9['Isophote']
    min_disc_z = 0.
    min_disc_radius = 0.
    psf_image = BASIC_PARS['psf_image'][0]
    
  PARS_BPS = pickle.load( open( bps_file, "rb" ) )

  [h_in_mean,h_in_neg,h_in_pos,m0d_in_mean,m0d_in_neg,m0d_in_pos] = PARS_BPS["INNER_DISC"]
  [h_out_mean,h_out_neg,h_out_pos,m0d_out_mean,m0d_out_neg,m0d_out_pos] = PARS_BPS["OUTER_DISC"]
  [R_break_mean,R_break_neg,R_break_pos] = PARS_BPS["BREAK_RADIUS"]
  [z0_gen] = PARS_BPS["THICKNESS"]
  [z_break_mean,z_break_neg,z_break_mean] = PARS_BPS["BREAK_Z"]
  [z0_thin_mean,z0_thin_neg,z0_thin_pos] = PARS_BPS["THIN_DISC"]
  [z0_thick_mean,z0_thick_neg,z0_thick_pos] = PARS_BPS["THICK_DISC"]
  [delta_m0d_thin_thick] = PARS_BPS["DELTA_M0D"]
  [R_bulge_r,R_bulge_z] = PARS_BPS["BULGE_DOM"]
  
  [h_d_gen,m0_d_gen,R_disc_min] = PARS_BPS["DISC_DOM"]
  min_disc_radius = R_disc_min


  IMAGES = [input_image,'none',psf_image,mask_image]
  RADII = [2*max_disc_radius,min_disc_radius,R_bulge_r,max_disc_z]


  R_break_mean = float('nan')
  z0_thin_mean = float('nan')
  z0_thick_mean = float('nan')
  delta_m0d_thin_thick = float('nan')
  h_in_mean = float('nan')
  m0d_in_mean = float('nan')
  h_out_mean = float('nan')
  m0d_out_mean = float('nan')


  #print psf_image
  #exit()
  DISCS = [R_break_mean,m0_d_gen,h_d_gen,z0_gen,z0_thin_mean,z0_thick_mean,delta_m0d_thin_thick,h_in_mean,m0d_in_mean, h_out_mean, m0d_out_mean]

  region_file = glob.glob("./analysis/"+gal_dir+"/ds9_contour/isophote_*.reg")[0]

  PARS_DEC = decomp_thin_thick_new.main_2D(IMAGES,RADII,DISCS,xc,yc,m0,pix2sec,mag_level=25.5,iso_reg=region_file,sampling=1)


  pickle.dump( PARS_DEC, open( dec_file, "wb" ) )
  files_after = glob.glob("*.*")
  move_files(files_after,directory="./analysis/"+gal_dir+"/dec")

elif action == 4:
  import Decomposition_eon

  if not os.path.exists("./analysis/"+gal_dir+"/disc_dec"):
    os.makedirs("./analysis/"+gal_dir+"/disc_dec")  

  BASIC_PARS = pickle.load( open( basic_file, "rb" ) )
  mag_level = BASIC_PARS['mag_level'][0]
  m0 = BASIC_PARS['m0'][0]
  pix2sec = BASIC_PARS['pix2sec'][0]

  if use_basic==True:
    xc = BASIC_PARS['xc'][0]
    yc = BASIC_PARS['yc'][0]
    max_disc_radius = BASIC_PARS['max_disc_radius'][0]
    min_disc_radius = BASIC_PARS['min_disc_radius'][0]
    max_disc_z = BASIC_PARS['max_disc_z'][0]
    min_disc_z = BASIC_PARS['min_disc_z'][0]
    psf_image = BASIC_PARS['psf_image'][0]   
  else:
    PARS_DS9 = pickle.load( open( ds9_file, "rb" ) )
    [xc,yc,max_disc_radius,max_disc_z,phi] = PARS_DS9['Isophote']
    min_disc_z = 0.
    min_disc_radius = 0.
    psf_image = BASIC_PARS['psf_image'][0]
    
  PARS_BPS = pickle.load( open( bps_file, "rb" ) )

  [h_in_mean,h_in_neg,h_in_pos,m0d_in_mean,m0d_in_neg,m0d_in_pos] = PARS_BPS["INNER_DISC"]
  [h_out_mean,h_out_neg,h_out_pos,m0d_out_mean,m0d_out_neg,m0d_out_pos] = PARS_BPS["OUTER_DISC"]
  [R_break_mean,R_break_neg,R_break_pos] = PARS_BPS["BREAK_RADIUS"]
  [z0_gen] = PARS_BPS["THICKNESS"]
  [z_break_mean,z_break_neg,z_break_mean] = PARS_BPS["BREAK_Z"]
  [z0_thin_mean,z0_thin_neg,z0_thin_pos] = PARS_BPS["THIN_DISC"]
  [z0_thick_mean,z0_thick_neg,z0_thick_pos] = PARS_BPS["THICK_DISC"]
  [delta_m0d_thin_thick] = PARS_BPS["DELTA_M0D"]
  [R_bulge_r,R_bulge_z] = PARS_BPS["BULGE_DOM"]
  
  [h_d_gen,m0_d_gen,R_disc_min] = PARS_BPS["DISC_DOM"]
  min_disc_radius = R_disc_min


  IMAGES = [input_image,'none',psf_image,mask_image]
  RADII = [2*max_disc_radius,min_disc_radius,max_disc_z]

  region_file = glob.glob("./analysis/"+gal_dir+"/ds9_contour/isophote_*.reg")[0]
  
  R_break_mean = float('nan')
  z0_thin_mean = float('nan')
  z0_thick_mean = float('nan')
  delta_m0d_thin_thick = float('nan')
  h_in_mean = float('nan')
  m0d_in_mean = float('nan')
  h_out_mean = float('nan')
  m0d_out_mean = float('nan')

  DISCS = [R_break_mean,m0_d_gen,h_d_gen,z0_gen,z0_thin_mean,z0_thick_mean,delta_m0d_thin_thick,h_in_mean,m0d_in_mean, h_out_mean, m0d_out_mean]

  PARS_DISC_DEC,output_file_sum,output_file_vert = Decomposition_eon.main_2D(IMAGES,RADII,DISCS,xc,yc,m0,pix2sec,mag_level=25.5,iso_reg=None,sampling=1,program='galfit')


  pickle.dump( PARS_DISC_DEC, open( disc_dec_file, "wb" ) )
  files_after = glob.glob("*.*")
  move_files(files_after,directory="./analysis/"+gal_dir+"/disc_dec")  

elif action == 5:
    import plot_isomap 
    BASIC_PARS = pickle.load( open( basic_file, "rb" ) )
    mag_level = BASIC_PARS['mag_level'][0]
    m0 = BASIC_PARS['m0'][0]
    pix2sec = BASIC_PARS['pix2sec'][0]
    
    input_data = '20,25,0.5'
    input_data = str(raw_input('Please input inner isophote, outer isophote and step using , (%s): ' % (input_data))) or input_data
      
    input_data = input_data.split(',')
    inner_iso = float(input_data[0])  
    outer_iso = float(input_data[1])
    step = float(input_data[2])  
    iso_file = plot_isomap.main(input_image,m0,pix2sec,inner_iso,outer_iso,step)
    os.rename(iso_file,iso_file.split('.')[0]+gal_dir+'.'+iso_file.split('.')[1])
    move_files([iso_file.split('.')[0]+gal_dir+'.'+iso_file.split('.')[1]],directory="./analysis/"+gal_dir)

elif action == 6:
    import iraf_fit_ellipse
    BASIC_PARS = pickle.load( open( basic_file, "rb" ) )
    mag_level = BASIC_PARS['mag_level'][0]
    m0 = BASIC_PARS['m0'][0]
    pix2sec = BASIC_PARS['pix2sec'][0]
    xc = BASIC_PARS['xc'][0]
    yc = BASIC_PARS['yc'][0]

    
    file1,file2 = iraf_fit_ellipse.main(input_image,xc,yc,m0,pix2sec,minsma=1.,step=3.)
    move_files([file1.split('/')[-1],file2.split('/')[-1]],directory="./analysis/"+gal_dir)
    files_after = glob.glob("*.par*")
    move_files(files_after,directory=None)

elif action == 9:
    import create_latex_pdf
    
    pictures = []
    isomap_pdf = glob.glob("./analysis/"+gal_dir+"/isomap*.pdf")[0]
    iraf_pdf = glob.glob("./analysis/"+gal_dir+"/*_iraf_ell.pdf")[0]
    hor_prof_pdf = glob.glob("./analysis/"+gal_dir+"/dec/*_prof_hor.pdf")[0]
    vert_prof_pdf = glob.glob("./analysis/"+gal_dir+"/dec/*_prof_ver.pdf")[0]
    model_pdf = glob.glob("./analysis/"+gal_dir+"/dec/model.pdf")[0]
    
    for file in [isomap_pdf,iraf_pdf,hor_prof_pdf,vert_prof_pdf,model_pdf]:
      pictures.append(file)
    create_latex_pdf.main(input_image.split('/')[-1].split('_')[0], pictures)
elif action == 10:
    BASIC_PARS = pickle.load( open( basic_file, "rb" ) )
    mag_level = BASIC_PARS['mag_level'][0]
    m0 = BASIC_PARS['m0'][0]
    pix2sec = BASIC_PARS['pix2sec'][0]
    D = BASIC_PARS['D'][0]
    Redsh = BASIC_PARS['Redsh'][0]
    Aext = BASIC_PARS['Aext'][0]
    Col = BASIC_PARS['Col'][0]
    Col_val = BASIC_PARS['Col_val'][0]


    PARS_DEC = pickle.load( open( dec_file, "rb" ) )
    #write_report(PARS, ff_out)

    read_dec_dict("./analysis/"+gal_dir+"/dec/composed_model.fits", PARS_DEC, pix2sec, m0, D, Redsh, Aext, Col, Col_val)
    shutil.move('dec_results.dat',"./analysis/"+gal_dir + '_dec_results.dat')
else:
    print 'The input action does not exist. Exiting...'
    exit()

