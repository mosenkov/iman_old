#!/usr/bin/env python
# -*- coding: utf8 -*-
# *****************************************************************
# **       PTS -- Python Toolkit for working with SKIRT          **
# **       © Astronomical Observatory, Ghent University          **
# *****************************************************************

## \package do.fitskirt_rotate
# IMPORTANT: should be run after fitskirt_skysub (or fitskirt_psf)
#
# This routine reads in a files list and 2 coordinates sets and then rotates
# the frame so the two sets are placed horizontally around the center.
# The first set is the left edge of the galaxy while
# the second set is the right edge of the galaxy.
#
# REQUIREMENTS: 
# - the files list should be free of ".fits" extension
# - the x-coordinate of the left edge
# - the y-coordinate of the left edge
# - the x-coordinate of the right edge
# - the y-coordinate of the right edge
#
# OPTIONAL:
# - boolean asking to flip the final result 180 degrees
#
# EXAMPLE: >pts fitskirt_rotate files.dat 484 881 1009 635 True
#
# This rotates all frames in files.dat to align 484,881 and 1009,635
# around the center (746,758) and flips the final result vertically.
#
# -----------------------------------------------------------------
 
# Import the necessary modules
import pyfits
import numpy as np
import math
import itertools
import matplotlib.pyplot as plt
from scipy import ndimage
from numpy import *
import sys
import subprocess
import shutil
import os
import glob

PATH_TO_SCRIPT = os.path.dirname(os.path.realpath(__file__))
PATH_TO_PACKAGE = os.path.split(PATH_TO_SCRIPT)[0].split('IMP_NEW')[0]
sys.path.append(PATH_TO_PACKAGE + '/Warps')

import Find_pos_angle

import imp_rebin
import imp_setup
from Sergey_pipelinelib.rotima import crotima
from astropy.stats import sigma_clipped_stats

# Colors to highlight the output text
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

def add_header(keywords, inputname, outputname):
    print "Writing main keywords to the header..."
    hdulist_ima = pyfits.open(inputname)
    data_image = hdulist_ima[0].data
    header = hdulist_ima[0].header
    
    hdulist_ima1 = pyfits.open(outputname)
    data_image1 = hdulist_ima1[0].data
    header1 = hdulist_ima1[0].header
    
    for keyword in keywords:
      if keyword in header:
	header1[keyword] = header[keyword] 
    hdu = pyfits.PrimaryHDU(data_image1,header1)
    hdu.writeto(outputname,clobber=True)

# Main function, this part actually runs when routine is called
def rotate_manual(fileToShow):
    method = imp_setup.manual_method
    if method == 'line' or method=='points':
	if not os.path.exists('./reg/rot.reg'):  
	  open("rot.reg", "a").close()
	else:
	  shutil.copy('./reg/rot.reg','rot.reg')
	p = subprocess.Popen(["ds9",fileToShow,"-scale","histequ","-cmap","Cool","-regions","load","rot.reg","-contour","limits"])
	p.wait()
	f = open("rot.reg", "r") 
	lines = f.readlines()
	if method=='line':
	    for line in lines:
	      if 'line(' in line:
		coords = np.array(line.split('(')[1].split(')')[0].split(','), float)
		x_l,y_l,x_r,y_r = coords
		break
	if method=='points':
	    side='left'
	    for line in lines:
	      if 'point(' in line:
		coords = np.array(line.split('(')[1].split(')')[0].split(','), float)
		if side=='left':
		  x_l,y_l = coords
		  side='right'
		else:
		  x_r,y_r = coords
		
	if x_l>x_r:
	  a = x_r
	  b = y_r
	  x_r = x_l
	  y_r = y_l
	  x_l = a
	  y_l = b
	
	left_x = int(ceil(x_l+0.5))
	left_y = int(ceil(y_l+0.5))
	right_x = int(ceil(x_r+0.5))
	right_y = int(ceil(y_r+0.5))
	xc = (left_x+right_x) / 2.
	yc = (left_y+right_y) / 2.
	PosAng = math.degrees(math.atan(float(left_y - right_y)/float(left_x - right_x)))
	f.close()
	if os.path.exists('rot.reg'):
	  shutil.move('rot.reg','./reg/rot.reg')
    if method=='zoom':
	ds9Proc = subprocess.Popen(["ds9",fileToShow,"-scale","histequ","-cmap","Cool","-contour","limits"])
      
	inputString = raw_input("Input xc,yc,PA: ")
	inputString = inputString.split(',')
	if inputString:
	    xc = float(inputString[0])
	    yc = float(inputString[1])
	    PosAng = float(inputString[2])
	else:
	    PosAng = 0.0
	if PosAng>270.:
	  PosAng = 360. - PosAng
	if PosAng>90. and PosAng<180.:
	  PosAng = 180. - PosAng
    return xc,yc,PosAng





def main(mode='m', inp_dir = 'sky_subtr/', outp_dir="./rot", ref_image=None, x_obj=None, y_obj=None, coords='world', m0=0., pix2sec=1):
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "        GALAXY IMAGE ROTATION 2016       " + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    m0 = float(m0)
    pix2sec = float(pix2sec)
    if not os.path.exists(outp_dir):
      os.makedirs(outp_dir)
    '''
    if os.path.exists("./align"):
      arr = glob.glob('align/*.fits')
    else:
      arr = glob.glob('sky_subtr/*.fits')
    '''
    arr = glob.glob('%s*.fits' % (inp_dir))
    
    if ref_image == None:
	ref_image = arr[0]

    if mode == 'a' or mode == 'sa':
      for i in range(len(arr)):
	  if ref_image in arr[i]:
	      if coords=='world':
		from astropy.wcs import WCS
		w = WCS(ref_image)
		[[xc,yc]] = w.wcs_world2pix([[x_obj,y_obj]], 1)

	      hdulist_ima = pyfits.open(ref_image)
	      data_image = hdulist_ima[0].data
	      header = hdulist_ima[0].header
	      if 'Sky_std' in header:
		std_backgr = float(header['Sky_std'])
	      else:
		I_mean, I_median, std_backgr = sigma_clipped_stats(data_image, sigma=3.0, iters=5)
	      
	      inner_level = m0 - 2.5*log10(imp_setup.inner_level * std_backgr) + 5.*log10(pix2sec)
	      outer_level = m0 - 2.5*log10(imp_setup.outer_level * std_backgr) + 5.*log10(pix2sec)
	      
	      PA,xcc,ycc,smma,smmb = Find_pos_angle.main(ref_image,m0,pix2sec,xc,yc,inner_level,outer_level)
	      x_l = xcc - fabs(cos(radians(PA))) * smma
	      y_l = ycc - fabs(sin(radians(PA))) * smma
	      x_r = xcc + fabs(cos(radians(PA))) * smma
	      y_r = ycc + fabs(sin(radians(PA))) * smma  
	      fout = open("rot.reg", "w")
	      fout.truncate(0)
	      fout.write("# Region file format: DS9 version 4.1\n")
	      fout.write('global color=green dashlist=8 3 width=1 font="helvetica 10 ')
	      fout.write('normal roman" select=1 highlite=1 dash=0 fixed=0 ')
	      fout.write('edit=1 move=1 delete=1 include=1 source=1\n')
	      fout.write('image\n')
	      fout.write("line(%1.1f,%1.1f,%1.1f,%1.1f)\n" % (x_l, y_l, x_r, y_r))
	      fout.close()
	      if mode=='sa':
		    xCenRot, yCenRot = crotima(ref_image, 'rot_tmp.fits',xc,yc,PA)
		    hdulist_tmp = pyfits.open('rot_tmp.fits')
		    data_tmp = hdulist_tmp[0].data
		    ny1,nx1 = np.shape(data_tmp)
		    fout = open("rot_tmp.reg", "w")
		    fout.truncate(0)
		    fout.write("# Region file format: DS9 version 4.1\n")
		    fout.write('global color=green dashlist=8 3 width=1 font="helvetica 10 ')
		    fout.write('normal roman" select=1 highlite=1 dash=0 fixed=0 ')
		    fout.write('edit=1 move=1 delete=1 include=1 source=1\n')
		    fout.write('image\n')
		    fout.write("line(%1.1f,%1.1f,%1.1f,%1.1f)\n" % (0.,yCenRot, nx1, yCenRot))
		    fout.close()    
		    p = subprocess.Popen(["ds9",'rot_tmp.fits',"-scale","histequ","-cmap","Cool","-regions","load","rot_tmp.reg","-contour","limits"])
		    p.wait()
		    inputString = raw_input("Correction to position angle: ").strip()
		    if inputString:
			deltaPosAng = float(inputString)
		    else:
			deltaPosAng = 0.0
		    PA = PA-deltaPosAng
		    os.remove("rot_tmp.reg")
		    os.remove("rot_tmp.fits")
	      shutil.move('rot.reg','./reg/rot.reg')
	      break
    else:
      for i in range(len(arr)):
	if ref_image in arr[i]:
	  xc,yc,PA = rotate_manual(arr[i])
	  break
    for i in range(len(arr)):
		inputname = arr[i]
		outputname = outp_dir+'/'+arr[i].split('/')[-1].split('.fits')[0]+"_rot.fits"
		xCenRot, yCenRot = crotima(inputname, outputname,xc,yc,PA)
		add_header(['Sky_std','Sky_med'],inputname,outputname)
		hdulist = pyfits.open(outputname, do_not_scale_image_data=True, mode='update')
		prihdr = hdulist[0].header
		prihdr.append(('PA',PA),end=True)
		hdulist.flush()
    print bcolors.OKGREEN+ 'Coordinates of the center are' + bcolors.ENDC, xc, yc
    print bcolors.OKGREEN+ 'Position angle is' + bcolors.ENDC, round(PA,3)    
    print 'Done!'
    return xc, yc,PA

'''
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Galaxy image rotation")
    parser.add_argument("--mode", nargs='?', const=1, help="Input the mode: a or m",type=str)
    parser.add_argument("--outp_dir", nargs='?', const=1, help="Input the output directory: ./rot",type=str,default='./rot')
    parser.add_argument("--ref_ima", nargs='?', const=1, help="Input the reference image",type=str)
    parser.add_argument("--x_obj", nargs='?', const=1, help="Optional: Input the x-coordinate (or RA) of the object where to estimate the sky",type=str,default='none') 
    parser.add_argument("--y_obj", nargs='?', const=1, help="Optional: Input the y-coordinate (or DEC) of the object where to estimate the sky",type=str,default='none') 
    parser.add_argument("--coords", nargs='?', const=1, help="Optional: Input the system of coordinates: world or pixels",type=str,default='world')
    parser.add_argument("--m0", nargs='?', const=1, help="Optional: Input the zero point",type=float,default=0.)
    parser.add_argument("--pix2sec", nargs='?', const=1, help="Optional: Input the pixel scale",type=float,default=1.)
    args = parser.parse_args()

    mode = args.mode
    outp_dir = args.outp_dir
    ref_image = args.ref_ima
    if args.x_obj=='none':
      x_obj = None
    else:
      x_obj = float(args.x_obj)
    if args.y_obj=='none':
      y_obj = None
    else:
      y_obj = float(args.y_obj)
    if args.manual=='yes':
      manual = True
    else:
      manual = False

    coords = args.coords
    m0 = args.m0
    pix2sec = args.pix2sec
    xc, yc,PA = main(mode, outp_dir=outp_dir, ref_image=ref_image, x_obj=x_obj, y_obj=y_obj,coords=coords, m0=m0, pix2sec=pix2sec)
    
'''

#xCenRot, yCenRot = crotima('H_final.fits', 'H_final_rot.fits',607,730,33.82)

#main('m', outp_dir="./rot", ref_image='sky_subtr/SDSS_g_a_r.fits_sub.fits', x_obj=181.76309, y_obj=43.06582, coords='world', m0=28.450, pix2sec=0.396)
#main('m', outp_dir="./rot", ref_image=None, x_obj=228.974042, y_obj=56.328771, coords='world', m0=29.38, pix2sec=0.834)