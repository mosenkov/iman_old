#!/usr/bin/python
# -*- coding:  cp1251 -*-
# *****************************************************************
# **       IMP -- Python Toolkit for image preparation           **
# **          to olygochromatic (multiband) fitting              **
# **           © Saint Petersburg State University               **
# **         Written by S. Savchenko and A. Mosenkov             **
# **      savchenko.s.s@gmail.com, mosenkovAV@gmail.com          **
# *****************************************************************
# Description: Script to determine PSF (using Moffat function). Several frames of the same object can be downloaded. The astrometrically corrected fits-images should be placed in the /raw directory. Only fits files should be found there!
# Type for help: $python imp.py -h

# Import standard modules
import sys
import math
import numpy as np
from scipy import stats
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import matplotlib.path as path
from matplotlib.ticker import NullFormatter
from numpy import *
from pylab import *
import os
import shutil
import subprocess
import random
import re
import glob
import argparse
import pyfits


import do_preparation

#*** Colour fonts ***
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''





def main(inputIMP = None, galaxy_numbers = None, steps = None, mode = None, pix2sec2 = None, RA = None, DEC = None, band=None):
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "                 IMP 2016                " + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    print inputIMP
    ## 1. Read in the input file (table) with objects OR by default consider objects located in /raw

    # If no input table is given, check if non-empty directory /raw exists
    if inputIMP == None:
	if not os.path.exists('./raw') or len(glob.glob('./raw/*.fits'))==0:
	  print bcolors.FAIL+'The default directory /raw does not exist or empty!' + bcolors.ENDC
	else:
	  # Do the job
	  # Create log-file
	  path_to_save_out = 'results.log'
	  f_out = open(path_to_save_out,'w')
	  print >>f_out, 'Number\tStatus'
	  do_preparation.main(f_out,steps[0],mode[0],pix2sec2,m0=None,xc=RA,yc=DEC,band=band,input_directory='./raw')
	  f_out.close()
	  exit()
    else:
	if not os.path.exists(inputIMP):
	    print bcolors.FAIL+'The input file %s is not found!' % (inputIMP) + bcolors.ENDC
	NUMBER,NAME,FILTER,PATH,STEP,MODE,XC,YC,Radius,PIX2SEC2,M0 = \
	loadtxt(inputIMP, usecols=[0,1,2,3,4,5,6,7,8,9,10],dtype=str, unpack=True,skiprows=1,delimiter='\t')      
	if len(NUMBER)==1:
	  NUMBER = np.array([NUMBER])
	  NAME = np.array([NAME])
	  FILTER = np.array([FILTER])
	  PATH = np.array([PATH])
	  STEP = np.array([STEP])
	  MODE = np.array([MODE])
	  XC = np.array([XC])
	  YC = np.array([YC])
	  Radius = np.array([Radius])
	  PIX2SEC2 = np.array([PIX2SEC2])
	  M0 = np.array([M0])


	# Create directory for output results
	if not os.path.exists("./results"):
	  os.makedirs("./results")
	  
	# Create log-file
	path_to_save_out = 'results.log'
	f_out = open(path_to_save_out,'w')
	print >>f_out, 'Number\tStatus'
	f_out.close()

	## 2. Copy images with the same number to /NUMBER[k]/raw:
	NUMBERS = []
	MODES = []
	STEPS = []
	PIX2SEC2S = []
	XCS = []
	YCS = []
	BANDS = []
	RADII = []
	NAMES = []
	
	for k in range(len(NUMBER)):
	  if galaxy_numbers!=None:
	      # Only galaxies from command line should be analyzed!
	      if NUMBER[k] not in galaxy_numbers:
		  continue
	  if STEP[k]=='99':
		  continue
	  if not os.path.exists("./results/%s/raw" % (NUMBER[k])):
	    os.makedirs("./results/%s/raw" % (NUMBER[k]))
	  if not os.path.exists("./results/%s/raw/%s.fits" % (NUMBER[k],NAME[k])):
	    print 'Copying the image...'

	    shutil.copy(PATH[k],"./results/%s/raw/%s.fits" % (NUMBER[k],NAME[k]))

	  # Add band name to the header
	  HDU = pyfits.open("./results/%s/raw/%s.fits" % (NUMBER[k],NAME[k]), mode='update')
	  level = 0
	  if len(HDU)>1:
	    level = str(raw_input('Please choose the HDU level to work with: %i? ' % (0)) or '0')
	  data = HDU[int(level)].data
	  header = HDU[int(level)].header
	  header['BAND'] = FILTER[k]
	  header['INI_NAME'] = PATH[k]
	  hdu = pyfits.PrimaryHDU(data,header)
	  hdu.writeto("./results/%s/raw/%s.fits" % (NUMBER[k],NAME[k]),clobber=True)

  
	  if NUMBER[k] not in NUMBERS:
	    NUMBERS.append(NUMBER[k])
	    MODES.append(MODE[k])
	    STEPS.append(STEP[k])
	    PIX2SEC2S.append(float(PIX2SEC2[k]))
	    XCS.append(float(XC[k]))
	    YCS.append(float(YC[k]))
	    BANDS.append(FILTER[k])
	    RADII.append(float(Radius[k]))
	    NAMES.append(NAME[k])

	## 3. Now working with each number:
	for k in range(len(NUMBERS)):
	    f_out = open(path_to_save_out,'a')
	    print '\n\n\n'
	    print bcolors.OKGREEN+NUMBERS[k],' ',NAMES[k] + bcolors.ENDC
	    
	    # 1. Change the directory
	    os.chdir("./results/%s" % (NUMBERS[k]))

	    # 2. Working inside
	    do_preparation.main(f_out,STEPS[k],MODES[k],PIX2SEC2S[k],M0[k],XCS[k],YCS[k],RADII[k],band=BANDS[k],input_directory='./raw')
	    os.chdir('..')
	    os.chdir('..')

	    # Add information to the log-file for the entire sample
	    f_out.close()




if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="DEComposition Analysis: IMage Preparation")   
    parser.add_argument("--i", default=None,
                    help="Input table for IMP")
    parser.add_argument("--g", default=None, nargs='+',
                    help="Select galaxies by number from the input file. Example: --g 1,3,8 ")

    parser.add_argument("--steps", default=None, nargs='+',
                    help="Select which steps you want to do. Example: --steps 0,1,4 ")

    parser.add_argument("--mode", default=None, nargs='+',
                    help="Select the mode for each step. Example: --mode a,sma,a ")
    # a --- fully automatic analysis
    # sma --- semi-automatic analysis
    # m --- fully manual analysis
    parser.add_argument("--pix2sec2", default=None, nargs='+',
                    help="Specify the scale in arcsec/pix the frames should be rebinned to.")
    parser.add_argument("--ra", default=None, nargs='+',
                    help="Specify RA of the object under study")
    parser.add_argument("--dec", default=None, nargs='+',
                    help="Specify DEC of the object under study")
    parser.add_argument("--b", default=None, nargs='+',
                    help="Specify photometric band")

    args = parser.parse_args()

    inputIMP = args.i

    if args.g!=None:
      galaxies = args.g
      steps = args.steps
      mode = args.mode
      pix2sec2 = args.pix2sec2

      galaxy_numbers = galaxies[0].split(',')

    else:
      galaxy_numbers = None
      steps = args.steps
      mode = args.mode
      pix2sec2 = args.pix2sec2
    RA = args.ra
    DEC = args.dec
    b = args.b
    main(inputIMP,galaxy_numbers,steps,mode,pix2sec2,RA,DEC,b)     









