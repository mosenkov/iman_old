#!/usr/bin/env python
# -*- coding: utf8 -*-
# *****************************************************************
# **       PTS -- Python Toolkit for working with SKIRT          **
# **       © Astronomical Observatory, Ghent University          **
# *****************************************************************

## \package do.fitskirt_center
# IMPORTANT: should be run after fitskirt_rotate
#
# This routine reads in a files list and one coordinate set and two lower bounds.
# It cuts out the frames respecting the bounds and placing the coordinate in the center.
# Additionally, a rebinningfactor can be chosen.
#
# REQUIREMENTS: 
# - the files list should be free of ".fits" extension
# - the x-coordinate of the center
# - the y-coordinate of the center
# - the x-coordinate of the lower bound on the x-coordinate (left) 
# - the y-coordinate of the lower bound on the y-coordinate (down) 
#
# OPTIONAL:
# - integer n used to rebin the frame by combining (n,n) to (1,1)
#
# EXAMPLE: >pts fitskirt_center files.dat 1007 992 612 920 2
#
# This centers the frame from 612 to 1402 in x (so 1007 is the center)
# and 920 to 1064 in y (so 992 is the center) and rebins by 2,2
#
# -----------------------------------------------------------------

# Import the necessary modules
import pyfits
import numpy as np
import math
import itertools
import matplotlib.pyplot as plt
from scipy import ndimage
from numpy import *
import sys
import subprocess
import shutil
import os
import glob
try:
  import astropy.io.fits as pyfits
  from astropy import wcs
except:
  warnings.warn("Astropy is not installed! No WCS has been added to the header!")

import imp_setup
import crea_psf
import imp_rebin
import sextr_psf_run
import sextr_psf_reg

# Colors to highlight the output text
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


def fwhm_read(input_image,survey):
    hdulist = pyfits.open(input_image)
    header = hdulist[0].header

    fwhm = 1.	# in arcsec

    if 'SEEING' in header:

        fwhm = float(header['SEEING'])
        if survey=='UKIDSS':
	  fwhm = fwhm*float(header['PIXLSIZE'])

    elif 'SEESH' in header and survey=='2MASS':
	# 2MASS
        fwhm = 3.13*float(header['SEESH']) - 0.46

    elif 'BAND' in header and survey=='WISE':
	# WISE
	band = int(header['BAND'])
	if band==1:
		fwhm = 6.08
	elif band==2:
		fwhm = 6.84
	elif band==3:
		fwhm = 7.36
	elif band==4:
		fwhm = 11.99

    elif survey=='SDSS':
	fwhm = 1.3

    else:
        print "Could not determine FWHM. Set to 1 arcsec."


    # Return FWHM (in arcseconds)
    return float(fwhm)  


def psf_manual(fileToShow, psf_reg):
    if not os.path.exists('./reg/'+psf_reg):  
      open(psf_reg, "a").close()
    else:
      shutil.copy('./reg/'+psf_reg,psf_reg)
      
    ## Display the image and select psf stars using circular annulus
    p = subprocess.Popen(["ds9",fileToShow,"-scale","histequ","-cmap","Cool","-regions","load",psf_reg,"-contour","limits"])
    p.wait()

'''
def crea_psf(input_image, region_file):    
    ## Fit selected stars with the gaussian or moffat function
    
    
    ## Chose the best model and create the final model psf_manual
    
    ## Crop the best star as an instrumental psf

    print 'here'
'''




def run_sextr(mode, input_image):
	psf_reg = input_image.split('/')[-1].split('.fits')[0] + '_psf.reg'
	# 1. Check if there are NaN values and fix them (set 0)
	try:
	      shutil.copy(input_image,'no_nan.fits') 
	      hdulist3 = pyfits.open('no_nan.fits', do_not_scale_image_data=True, mode='update')
	      img3 = hdulist3[0].data
	      img3[np.isnan(img3)]=0
	      hdulist3.flush()
	except:
	      # Try another fits layer: UKIDSS
	      hdulist3 = pyfits.open(input_image)
	      img3 = hdulist3[1].data
	      head = hdulist3[1].header
	      img3[np.isnan(img3)]=0
	      hdu = pyfits.PrimaryHDU(img3,head)
	      hdu.writeto('no_nan.fits',clobber=True)     

	# 2. Run SExtractor to catalogize stars
	try:
	  survey = input_image.split('/')[-1].split('_')[0]
	except:
	  survey = 'None'
	fwhm = fwhm_read('no_nan.fits',survey)
	pix2sec,note = imp_rebin.resolution(input_image)
	pix2sec = float(pix2sec)
	sextr_psf_run.run_sextr('no_nan.fits',22.5,4.,pix2sec,fwhm,'frame.cat')

	if mode=='sa':
	  # 3. Build a region file with the selected stars
	  hdulist = pyfits.open('no_nan.fits')
	  header = hdulist[0].header  
	  NX = header['NAXIS1']
	  NY = header['NAXIS2']
	  fail = sextr_psf_reg.main('frame.cat',psf_reg,NX,NY)
	  shutil.move(psf_reg,'./psf/'+psf_reg)






def main(mode='m', inp_dir='raw/', outp_dir="./psf", ref_image=None, x_obj=None, y_obj=None, coords='world', pix2sec=1):
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "          PSF DETERMINATION 2016         " + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC

    if not os.path.exists(outp_dir):
      os.makedirs(outp_dir)
    '''
    if os.path.exists("./raw"):
      arr = glob.glob('raw/*.fits')
    '''

    arr = glob.glob('%s*.fits' % (inp_dir))


    if mode == 'm':
      for i in range(len(arr)):
	  print bcolors.OKGREEN+ arr[i] + bcolors.ENDC
	  pix2sec_ima,note = imp_rebin.resolution(arr[i])
	  factor = pix2sec / float(pix2sec_ima)
	  region_file = arr[i].split('/')[-1].split('.fits')[0] + '_psf.reg'
	  run_sextr(mode, arr[i])
	  psf_manual('aper.fits',region_file)
	  os.remove('aper.fits')
	  crea_psf.main(arr[i],region_file,float(pix2sec_ima),factor,mode)
    if mode == 'sa':
      for i in range(len(arr)):
	  print bcolors.OKGREEN+ arr[i] + bcolors.ENDC
	  pix2sec_ima,note = imp_rebin.resolution(arr[i])
	  factor = pix2sec / float(pix2sec_ima)
	  region_file = arr[i].split('/')[-1].split('.fits')[0] + '_psf.reg'
	  run_sextr(mode, arr[i])
	  psf_manual('aper.fits',region_file)
	  os.remove('aper.fits')
	  crea_psf.main(arr[i],region_file,float(pix2sec_ima),factor,mode)
    if mode == 'a':
      for i in range(len(arr)):
	  # This mode should be used if only reg files already exist!
	  print bcolors.OKGREEN+ arr[i] + bcolors.ENDC
	  pix2sec_ima,note = imp_rebin.resolution(arr[i])
	  factor = pix2sec / float(pix2sec_ima)
	  region_file = arr[i].split('/')[-1].split('.fits')[0] + '_psf.reg'
	  if os.path.exists('./psf/'+region_file):
	    check_mode=False
	    if check_mode==True:
	      #print 'Looking at', './psf/'+region_file
	      #p = subprocess.Popen(["ds9",arr[i],"-scale","histequ","-cmap","Cool","-regions","load",'./psf/'+region_file,"-contour","limits"])
	      #p.wait()

	      
	      best_star_reg = arr[i].split('/')[-1].split('.fits')[0] + "_best_star.reg"
	      print 'Looking at', './psf/'+best_star_reg
	      #open(best_star_reg, "a").close()
	      p = subprocess.Popen(["ds9",arr[i],"-scale","histequ","-cmap","Cool","-regions","load",'./psf/'+best_star_reg,"-contour","limits"])
	      p.wait()
	      #shutil.move(best_star_reg,'./psf/'+best_star_reg)

	    else:
	      run_sextr(mode, arr[i])
	      os.remove('aper.fits')
	      best_star_reg = arr[i].split('/')[-1].split('.fits')[0] + "_best_star.reg"
	      shutil.copy('./psf/'+region_file, region_file)
	      shutil.copy('./psf/'+best_star_reg, best_star_reg)    
	      crea_psf.main(arr[i],region_file,float(pix2sec_ima),factor,mode)
	  else:
	    print bcolors.FAIL+'This mode should be used if only reg files already exist!' + bcolors.ENDC
	    exit()
    
    print 'Done!'

if __name__ == '__main__':
  main(mode='m',pix2sec=0.834)