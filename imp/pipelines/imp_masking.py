#!/usr/bin/env python
# -*- coding: utf8 -*-
# *****************************************************************
# **       PTS -- Python Toolkit for working with SKIRT          **
# **       © Astronomical Observatory, Ghent University          **
# *****************************************************************

## \package do.fitskirt_center
# IMPORTANT: should be run after fitskirt_rotate
#
# This routine reads in a files list and one coordinate set and two lower bounds.
# It cuts out the frames respecting the bounds and placing the coordinate in the center.
# Additionally, a rebinningfactor can be chosen.
#
# REQUIREMENTS: 
# - the files list should be free of ".fits" extension
# - the x-coordinate of the center
# - the y-coordinate of the center
# - the x-coordinate of the lower bound on the x-coordinate (left) 
# - the y-coordinate of the lower bound on the y-coordinate (down) 
#
# OPTIONAL:
# - integer n used to rebin the frame by combining (n,n) to (1,1)
#
# EXAMPLE: >pts fitskirt_center files.dat 1007 992 612 920 2
#
# This centers the frame from 612 to 1402 in x (so 1007 is the center)
# and 920 to 1064 in y (so 992 is the center) and rebins by 2,2
#
# -----------------------------------------------------------------

# Import the necessary modules
import pyfits
import numpy as np
import math
import itertools
import matplotlib.pyplot as plt
from scipy import ndimage
from numpy import *
import sys
import subprocess
import shutil
import os
import glob
import argparse
try:
  import astropy.io.fits as pyfits
  from astropy import wcs
except:
  warnings.warn("Astropy is not installed! No WCS has been added to the header!")

import imp_setup
import mask_objects
import backEst
import arithm_operations
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import convert_segm_to_region
# Colors to highlight the output text
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


def crea_comp_image(arr):
	from itertools import product
	kk = 0
	for ar in arr:
		inputname = ar
		hdulist = pyfits.open(inputname)
		primhdr = pyfits.getheader(inputname, 0)
		inframe = hdulist[0].data
		
		# Loop over the image and all polygons and fill them with the mask value	
		nx, ny =inframe.shape[1], inframe.shape[0]
		
		# Normalise the frame and write it out
		total = inframe.sum()

		for i, j in product(range(0,nx), range(0,ny)):
			inframe[j][i]=inframe[j][i]/total
		if kk==0:
		  INFRAME = inframe
		  kk = 1
		else:
		  INFRAME = INFRAME + inframe
	hdu = pyfits.PrimaryHDU(INFRAME)
	hdu.writeto('composite_ima.fits',clobber=True)	



def mask_manual(arr):
	if not os.path.exists('./reg/mask.reg'):  
	  open("mask.reg", "a").close()
	else:
	  shutil.copy('./reg/mask.reg','mask.reg')
	
	if imp_setup.mask_comp=='no':
	  print '\n\tIMAGES:'
	  
	  k = 0
	  for ar in arr:
	    k = k + 1
	    print '\t',k,'\t',ar.split('/')[1]

	  if len(arr)>1:
	    N_beg = 0
	    N_beg = int(input('Please choose the image to begin with (%i): ' % (N_beg+1))) - 1
	    seq = [N_beg]+range(0,N_beg)+range(N_beg+1,len(arr))
	  else:
	    seq = [0]
	  for ima in seq:
	    image = arr[int(ima)]
	    print image.split('/')[1]
	    p = subprocess.Popen(["ds9",image,"-scale","histequ","-invert","-regions","load","mask.reg"])
	    p.wait()
	else:
	  crea_comp_image('./ext/',"./rebin/files.dat",1,1,2)
	  p = subprocess.Popen(["ds9",'composite_ima.fits',"-scale","log","-cmap","Rainbow","-regions","load","mask.reg"])
	  p.wait()
	  os.remove('composite_ima.fits')      
	shutil.move("mask.reg","./reg/mask.reg")
      

def auto_mask(input_image, output_region_file, sextr_setup=None, models=False, galaxy_polygon=None, snr=2.):
    """Function creates ds9 *.reg file with regions that cover
    all objects except the galaxy"""
    
    backEst.call_SE(input_image, both=False, sextr_setup=sextr_setup, models=models, snr=snr)

    # find SE number of our galaxy (it is in the centre of the image now
    # so we can just get the intensity of the central pixel)

    if models==True:
      arithm_operations.main(input_image, 'objects.fits', 'sub', 'gal_sub.fits')
      backEst.call_SE(input_image, both=True, models=False)
      os.remove('gal_sub.fits')
      os.remove('objects.fits')

    if galaxy_polygon==None:
	segmHDU = pyfits.open("segm.fits")
	segmData = segmHDU[0].data
	ySize, xSize = segmData.shape
	xCen = int(xSize / 2.0)
	yCen = int(ySize / 2.0)
	galN = int(segmData[yCen, xCen])
	fout = open(output_region_file, "w")
	fout.truncate(0)
	fout.write("# Region file format: DS9 version 4.1\n")
	fout.write('global color=green dashlist=8 3 width=1 font="helvetica 10 ')
	fout.write('normal roman" select=1 highlite=1 dash=0 fixed=0 ')
	fout.write('edit=1 move=1 delete=1 include=1 source=1\n')
	fout.write('image\n')
	for line in open("field.cat"):
	    if line.startswith("#"):
		continue
	    params = line.split()
	    n = int(params[0])
	    if n == galN:
		continue
	    xCen = float(params[1])
	    yCen = float(params[2])
	    kron = float(params[8])
	    ellA = kron * float(params[4]) * imp_setup.exend_factor_mask
	    ellB = kron * float(params[5]) * imp_setup.exend_factor_mask
	    PA = float(params[6])
	    fout.write("ellipse(%1.1f,%1.1f,%1.1f,%1.1f,%1.1f)\n" % (xCen, yCen,
								    ellA, ellB,
								    PA))
	fout.close()
    
    else:
      # Convert segm to region
      convert_segm_to_region.main('segm.fits', 'mask_tmp.reg')

      ff = open(output_region_file, 'w')
      ff.write('image\n')
      f = open('mask_tmp.reg', "r")
      for line in f:
	 if 'polygon(' in line:
		coords = line.replace('(',',').replace(')',',').split(',')[1:-1]
		pol = []
		for kk in range(0,len(coords)-1,2):
		  pol.append((int(float(coords[kk])),int(float(coords[kk+1]))))
		polygon = Polygon(pol)
		if not galaxy_polygon.intersects(polygon):
		  ff.write(line)
      ff.close()
      f.close()
      
def mask_sa(arr):
	if not os.path.exists('./reg/mask.reg'):  
	  open("mask.reg", "a").close()
	else:
	  shutil.copy('./reg/mask.reg','mask.reg')
	
	if imp_setup.mask_comp=='no':
	  print '\n\tIMAGES:'
	  
	  k = 0
	  for ar in arr:
	    k = k + 1
	    print '\t',k,'\t',ar.split('/')[1]

	  if len(arr)>1:
	    N_beg = 0
	    N_beg = int(input('Please choose the image to begin with (%i): ' % (N_beg+1))) - 1
	    seq = [N_beg]+range(0,N_beg)+range(N_beg+1,len(arr))
	  else:
	    seq = [0]
	  kkk = 0
	  for ima in seq:
	    image = arr[int(ima)]
	    print image.split('/')[1]
	    if kkk==0:
	      print 'auto mask'
	      auto_mask(image,'mask.reg')
	      kkk = 1
	    p = subprocess.Popen(["ds9",image,"-scale","histequ","-invert","-regions","load","mask.reg"])
	    p.wait()
	else:
	  crea_comp_image(arr)
	  auto_mask('composite_ima.fits','mask.reg')
	  p = subprocess.Popen(["ds9",'composite_ima.fits',"-scale","log","-cmap","Rainbow","-regions","load","mask.reg"])
	  p.wait()
	  os.remove('composite_ima.fits')      
	shutil.move("mask.reg","./reg/mask.reg")


def mask_a(arr,ref_image=None):
	if not os.path.exists('./reg/mask.reg'):  
	  open("mask.reg", "a").close()
	else:
	  shutil.copy('./reg/mask.reg','mask.reg')
	
	if imp_setup.mask_comp=='no' and ref_image!=None:
	  auto_mask(ref_image,'mask.reg')
	else:
	  crea_comp_image(arr)
	  auto_mask('composite_ima.fits','mask.reg')
	  os.remove('composite_ima.fits')      
	shutil.move("mask.reg","./reg/mask.reg")


def main(mode='m', inp_dir='ext/', outp_dir="./mask", ref_image=None):
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "                 MASKING 2016            " + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC

    if not os.path.exists(outp_dir):
      os.makedirs(outp_dir)
    '''
    if os.path.exists("./ext"):
      arr = glob.glob('ext/*.fits')
    '''
    arr = glob.glob('%s*.fits' % (inp_dir))
    
    if ref_image == None:
	ref_image = ''


    if mode == 'm':
      mask_manual(arr)
    if mode =='sa':
      mask_sa(arr)
    if mode =='a':
      mask_a(arr,ref_image=None)


    mask_objects.main(arr,'./reg/mask.reg')
    print 'Done!'

def normalization(inp_dir='mask/', outp_dir="./norm"):
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "                 Auto Masking 2016            " + bcolors.ENDC
    print '\t\t\t',bcolors.OKBLUE+ "*****************************************" + bcolors.ENDC
    if not os.path.exists(outp_dir):
      os.makedirs(outp_dir)

    arr = glob.glob('%s*_clean.fits' % (inp_dir))
    for k in range(len(arr)):
      HDU = pyfits.open(arr[k])
      Data = HDU[0].data
      header = HDU[0].header

      hdu = pyfits.PrimaryHDU(Data/np.sum(Data), header)
      hdu.writeto(outp_dir+'/'+arr[k].split('/')[-1].split('.fits')[0]+'_norm.fits',clobber=True)
      
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Masking")
    parser.add_argument("input_image", help="Input fits image")
    parser.add_argument("output_region_file", help="Output region file")
    parser.add_argument("--snr", nargs='?', const=2., help="Optional: Input the threshold level above the background",type=float,default=2.)
    args = parser.parse_args()

    input_image = args.input_image
    output_region_file = args.output_region_file  
    snr = args.snr
    auto_mask(input_image, output_region_file, sextr_setup=None, models=False, galaxy_polygon=None, snr=snr)
