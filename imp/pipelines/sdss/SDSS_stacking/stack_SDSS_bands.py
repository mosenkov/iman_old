#! /usr/bin/env python

# See for details: https://www.aanda.org/articles/aa/pdf/2011/12/aa16716-11.pdf

import imcombine
import numpy as np
import sys
import os
import shutil

sys.path.append('/home/amosenko/MyPrograms/IMAN/IMP_NEW')
sys.path.append('/home/amosenko/MyPrograms/IMAN/Plot')
import mask_indiv
import subprocess
import imp_masking
import gauss_filter
import overlay_fits_rgb
import plot_fits_image
import pyfits
import subprocess

from PIL import Image

def read_m0(input_file):
	hdulist = pyfits.open(input_file)
	header = hdulist[0].header
	return float(header['M0'])

def main():
    if not os.path.exists('./png'):
        os.mkdir('./png')

    if not os.path.exists('./eps'):
        os.mkdir('./eps')
        
    name,ra,dec,radius = np.loadtxt('coordinates.dat', usecols=[0,1,2,3],dtype=str, unpack=True,skiprows=0)
    NAME = np.loadtxt('sample.dat', usecols=[0],dtype=str, unpack=True,skiprows=0)
    current_dir = os.getcwd()
    for k in range(len(name)):
     if k+1==24:
      # Dont apply gauss: 6,10,11,12,16,19, 30, 32
      if k+1==15 or k+1==22:
          SIGMA = 4.
      else:
          SIGMA = 2.
      
      if True:
        # 1. Mosaicing of the fields (MONTAGE or SWarp)
        
        
        # 2. Rebin images (to the same WCS)
        
        
        # 3. Converting to the same resolution (PSF convolution)
        
        
        # 4. Stacking g,r and i to improve SDSS (IRAF/imcombine)

        input_paths = []
        bands = ['g','r','i']
        '''
        for band in bands:
            input_paths.append('./downloads/%s/%s_%s_trim.fits' % (name[k],name[k],band))
        imcombine.imcombine(input_paths, './downloads/%s/%s_%s.fits' % (name[k],name[k],"".join(bands)), 0.1, 0.1)
        '''
        
        # 5. Subtraction of stars (SExtractor)
        os.chdir('./downloads/%s/' % (name[k]))

        input_image = '%s_%s.fits' % (name[k],"".join(bands))
        '''
        imp_masking.auto_mask(input_image, 'mask.reg', sextr_setup=None, models=False, galaxy_polygon=None)

        mask_indiv.additional_mask(input_image,xc=None,yc=None,FWHM=4.,output_mask='add_mask.reg',sigma_numb=9.0,xmin=None,ymin=None,xmax=None,ymax=None,box_size=5,median=None,std=None, galaxy_polygon=None, masking=None)


        filenames = ['mask.reg', 'add_mask.reg']
        with open('final_mask.reg', 'w') as outfile:
		outfile.write('image\n')
		for fname in filenames:
		    with open(fname) as infile:
			Lines = infile.readlines()
			for Line in Lines:
			  if 'image' not in Line:
			    outfile.write(Line)
                
        p = subprocess.Popen(["ds9",input_image,"-scale","log","-cmap","Rainbow","-regions","load","final_mask.reg"])
	p.wait()
        '''
        
        '''
        if not os.path.exists("ellipse.reg"):
            f = open("ellipse.reg","w")
            f.close()
        
        p = subprocess.Popen(["ds9",'gri_gauss.fits',"-scale","log","-cmap","Rainbow","-regions","load","ellipse.reg"])
	p.wait()
	'''
	
        if os.path.exists("ellipse.reg"):
            os.rename("ellipse.reg","circle.reg")
        
        f = open('circle.reg', "r")
        for line in f:
	    if 'circle(' in line:
	      xc = float(line.split('(')[-1].split(',')[0])
	      yc = float(line.split('(')[-1].split(',')[1])
	      radius = float(line.split(',')[2].split(')')[0])
	f.close()

        # Apply masks
        output_clean_image = 'gri_clean.fits'
        output_mask = 'mask.fits'
        mask_indiv.mask(input_image, 'final_mask.reg', factor=1, output_image=output_clean_image, output_mask=output_mask, show_running=True)
        
        output_image = 'gri_gauss.fits'
        # 6. Gaussian filtering
        gauss_filter.main(output_clean_image, output_image, SIGMA, merge=False) # For 15,22:4

        # 7. Creation of and devision by an artificial flatfield image
        
        blue_image = '%s_g_trim.fits' % (str(k+1))
        green_image = '%s_r_trim.fits' % (str(k+1))
        red_image = '%s_i_trim.fits' % (str(k+1))
        output_file = '%s.png' % (str(k+1))
        m0_blue = read_m0(blue_image)
        m0_green = read_m0(green_image)
        m0_red = read_m0(red_image)
        ZP = (m0_blue+m0_green+m0_red)/3.        
        Name = NAME[k]

	hdulist = pyfits.open(output_image)
	data = hdulist[0].data
	header = hdulist[0].header
	ny,nx = np.shape(data)
	xc = nx/2.
	yc = ny/2.
	
	for kk in range(ny):
            for ii in range(nx):
              if data[kk,ii]<0.:
                data[kk,ii]=0.
        
        if k+1==24:
            zoom = 9
            y_min = int(yc-yc/zoom)
            y_max = int(yc+yc/zoom)
            x_min = int(xc-xc/zoom)
            x_max = int(xc+xc/zoom)
        else:
            #radius = 0.7*60./0.396
            x_min=int(xc-radius); y_min=int(yc-radius)
            x_max=int(xc+radius); y_max=int(yc+radius)
        
        if k+1 in [8,10,22,24,26,28,29,30]:
            outer_level = 28.
        elif k+1 in [32]:
             outer_level = 26.5
        else:
            outer_level = 27.

        plot_fits_image.main(output_image, output_file, m0=ZP,pix2sec=0.396, outer_level=outer_level, sigma=None,text=Name, x_l=x_min, y_l=y_min, x_r=x_max, y_r=y_max, same_fov=True, stretch='arcsinh',Distance=None,plot_scale_size=False,L_bar=10.)
        cmd = '/usr/bin/convert %s.png %s.eps' % (str(k+1),str(k+1))
        subprocess.call(cmd, shell=True)
        shutil.copy(output_file,current_dir+'/png/'+ output_file)
        output_file = '%s.eps' % (str(k+1))
        
        
        shutil.copy(output_file,current_dir+'/eps/'+ output_file)

        os.chdir(current_dir)    
        #exit()
main()    
