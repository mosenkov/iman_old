import numpy as np
import scipy.ndimage as ndimage
import matplotlib.pyplot as plt
import sys
import pyfits

def main(input_image, output_image, sigma, merge=False):
    hdulist = pyfits.open(input_image, do_not_scale_image_data=True, mode='update')
    img = hdulist[0].data
    header = hdulist[0].header
    
    img_new = ndimage.gaussian_filter(img, sigma=sigma, order=0)
    if merge==True:
        outHDU = pyfits.PrimaryHDU(data=img_new+img, header=header)
    else:    
        outHDU = pyfits.PrimaryHDU(data=img_new, header=header)
    outHDU.writeto(output_image, clobber=True)

if __name__ == "__main__":
    input_image = sys.argv[1]
    output_image = sys.argv[2]
    sigma = float(sys.argv[3])
    main(input_image, output_image, sigma)