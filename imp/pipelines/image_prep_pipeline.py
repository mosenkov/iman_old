#!/usr/bin/python

# Import the necessary modules
import pyfits
import numpy as np
import math
import itertools
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from scipy import ndimage
import sys
from matplotlib.path import Path
from itertools import product
from math import hypot, cos, sin, radians, pi
from numpy import linspace, sign, zeros_like
import shutil
import argparse
import imp_setup
import os
import subprocess
from astropy.modeling import models, fitting
from astropy.stats import sigma_clipped_stats
from photutils import data_properties, properties_table
import numpy.ma as ma


from photutils import detect_threshold
from astropy.convolution import Gaussian2DKernel
from astropy.stats import gaussian_fwhm_to_sigma
from photutils import detect_sources




import auto_masking
import crop_galaxy_image
import rebin_image
import get_galaxy_ellipse
import mask_objects
import determine_sky
import mask_galaxy
import rotate_image




def main(galaxy_name, instrument, band, galaxy_ellipse, galaxy_WISE_decomposition, operation):
    # Define output data:
    output_image = 

    
    
    if operation==1:
        # CREATE REFERENCE IMAGE (CROP)
        crop_galaxy_image.main(input_image, 'reference.fits', xc, yc, 2.*sma, 2.*smb, PA, hdu=0, method='Montage', square=False) 

    if operation==2:
        # REBINNING
        rebin_image.rebin('reference.fits', input_image, output_image, hdu_ref=0, hdu_inp=0, preserve_bad_pixels=True)

    if operation==3:
        # AUTO-MASKING
        auto_masking.main(input_image, output_region_file, snr=2., region_type='polygon', sextr_setup=None, galaxy_ellipse=None)

    if operation==4:
        # DETERMINE MORE PRECISELY GALAXY PEREMETERS (OUTER ELLIPSE)
        position,a,b,theta =  = get_galaxy_ellipse.determine_galaxy_ellipse(input_image, segm_image, xc=None, yc=None, snr=2., min_radius=10.)
        galaxy_ellipse = [ [ [position[0], position[1]], a, b, theta]]

    if operation==5:
        # MASK GALAXY ON THE SEMNETATION MAP
        mask_objects.add_galaxy_mask(input_mask_image, output_mask_image, galaxy_ellipse, sma_in_factor=1.250, sma_out_factor=1.601)

    if operation==6:
        # DETERMINE and SUBTRACT SKY
        determine_sky.sky_subtraction(input_image, mask_omage, polynomial_degree=5, output_image='sky_subtr.fits', output_sky=None, hdu_inp=0)

    if operation==7:
        # DETERMINE PSF

    if operation==8:
        # ROTATE
        #montage_wrapper.commands.mRotate('cut.fits', 'cut_rot.fits', debug_level=None, status_file=None, rotation_angle=45., ra=214.18662, dec=35.982116, xsize=None, ysize=None) # DOES NOT KEEP WCS
        rotate_image.main(input_image, output_image, angle, hdu_inp=0)

    if operation==9:
        # FINAL CROPPING
        crop_galaxy_image.main(input_image, output_image, xc, yc, 1.601*sma, 1.601*smb, PA, hdu=0, method='Montage', square=False)

    if operation==10:
        # FINAL MASKING and CREATION OF THE INTERPOLATED IMAGE
        sigma=2.0
        factor_enlarge_masks = 1.4
        mask_galaxy.main(input_image, x0, y0, sma, q, PA, snr_outer=2., sigma_inner=sigma, enlarge_masks=factor_enlarge_masks, enlarge_masks_type='multiply')

    if operation==11:
        # CREATE SIGMA IMAGE

    if operation==12:
        # DEPROJECTION

    if operation==13:
        # RESAMPLING

    if operation==14:
        # DECA MODELLING



if __name__ == '__main__':


input_image = '/Users/mosenkov/CurrentWork/Test_Pipeline/NGC5529_SDSS_r.fits'  
xc = 1997.
yc = 2010.
sma = 368.024235545 / 0.45
q = 1./4.97486278969
smb = sma*q
PA = 23.4680577545











