#!/usr/bin/python
# -*- coding:  cp1251 -*-


# Import standard modules
import sys
import math
import numpy as np
from scipy import stats
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import matplotlib.path as path
from matplotlib.ticker import NullFormatter
from numpy import *
from pylab import *
import os
import shutil
import subprocess
import random
import re
import glob

import pyfits

from astroquery.simbad import Simbad
from astropy import coordinates
import astropy.units as u
from astropy import wcs

from astroquery.vizier import Vizier
from photutils import aperture_photometry
from photutils import CircularAperture

import find_zp



def main(band):
  arr = glob.glob('psf/*.psf')

  for ar in arr:
    # Read sky_background level (in DN):
    Mag,FWHM,Chi2,x0,y0,sky_backgr = loadtxt(ar, usecols=[1,2,6,7,8,9],dtype=float, unpack=True,skiprows=7,delimiter='\t')
    Star_Flux_Galfit = 10**(0.4*(22.5-Mag))

    
    filename = './raw/' + ar.split('/')[-1].split('.psf')[0]+'.fits'
    pixcrd = []
    for i in range(len(x0)):
      pixcrd.append([x0[i],y0[i]])
    pixcrd = np.array(pixcrd)
    psf_reg = 'psf/'+ar.split('/')[-1].split('.psf')[0] + '_psf.reg'
    f = open(psf_reg, "r") 
    lines = f.readlines()
    R = []

    for line in lines:
      if 'circle(' in line:
	R.append(float(line.split(',')[2].split(')')[0]))
      if 'ellipse(' in line or 'annulus(' in line:
	R.append(float(line.split(',')[2]))
    f.close()
    #print filename,band,pixcrd,R,sky_backgr
    #exit()
    find_zp.main(filename,band,pixcrd,R,sky_backgr)

if __name__ == '__main__':
  band = sys.argv[1]
  print band
  main(band)