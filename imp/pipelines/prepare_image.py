#!/usr/bin/python

from astropy.io import fits as pyfits
import numpy as np
import math
import itertools
import matplotlib.pyplot as plt
from scipy import ndimage
import sys
from matplotlib.path import Path
from itertools import product
from math import hypot, cos, sin, radians, pi
from numpy import linspace, sign, zeros_like
import shutil
import argparse
import os
import subprocess
from astropy.modeling import models, fitting
from astropy.stats import sigma_clipped_stats
#from photutils import data_properties, properties_table
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from astropy import wcs
from astroquery import ned

LOCAL_DIR = "/imp/pipelines"
IMAN_DIR = os.path.dirname(__file__).rpartition(LOCAL_DIR)[0]

sys.path.append(os.path.join(IMAN_DIR, 'detect_objects'))
sys.path.append(os.path.join(IMAN_DIR, 'decomposition/simple_fitting'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/masking'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/sky_fitting'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/cropping'))
sys.path.append(os.path.join(IMAN_DIR, 'Ellipse_photometry'))
sys.path.append(os.path.join(IMAN_DIR, 'plotting/1dprofile'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/psf'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/rebinning'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/add_astrometry'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/phot_calibration'))
sys.path.append(os.path.join(IMAN_DIR, 'imp'))

import run_SExtractor
import convert_segm_to_region
import convert_reg_to_mask
import auto_masking
import determine_sky
import get_galaxy_ellipse
import azimProfile
import plot_azim_averaged_profile
import crop_galaxy_image
import azim_aver_masking
import get_galaxy_center_ned
import model_masking
import merge_masks
import inner_masking
import crop_psf_star
import add_keyw_to_header
import rebin_image
import sersic_fitting
import add_wcs
import calibration
import sky_around_galaxy

from IMP import IMP_main



def prep_for_galfit(xc, yc):
    hdulis = pyfits.open('galaxy_clean_galf.fits')
    prihdr = hdulis[0].header

    pix2sec,note = rebin_image.resolution('galaxy_clean_galf.fits')
    pix2sec = float(pix2sec)  

    
    
    FWHM = float(prihdr['PSF_FWHM'])
    m0 = float(prihdr['ZP'])
    RON = 10.
    NCOMBINE = 1

    if 'EGAIN' in prihdr.keys():
        GAIN = prihdr['EGAIN']
    else:
        GAIN = 1.

    if 'EXPTIME' in prihdr.keys():
        EXPTIME = prihdr['EXPTIME']
    else:
        EXPTIME = 1.

    add_keyw_to_header.add_to_header('galaxy_clean_galf.fits',EXPTIME,GAIN,NCOMBINE,RON,m0,pix2sec,FWHM,sky_level=0.,sky_subtr=1,xc=xc,yc=yc)
    return m0,pix2sec




def main(galaxy_name, directory, input_image='new-image.fits', steps=[]):        
    # Go to the folder
    os.chdir('/home/amosenko/Toshiba_1/CurrentWork/Rich/test_wcs/%s' % (directory))

    if -1 in steps:
            ds9Proc = subprocess.Popen(["ds9", 'sky_subtr.fits',
                                        "-scale", "hist"])
            ds9Proc.wait()
            exit()

    if 0 in steps:
        # 0. Create PSF image
        IMP_main(input_image, 'crea_psf', region_file='best_stars.reg', psf_image='psf.fits', user_interact = True, number=None) # You can choose your own number of PSF star if needed
    
    if 5 in steps or 6 in steps:
        # 1. Determine galaxy center
        xc,yc = get_galaxy_center_ned.main(input_image, name=galaxy_name, RA=None, DEC=None)
    
    if 2 in steps:
        # 2. Do initial masking of the images
        IMP_main(input_image, 'image_masking', user_interact = False, region_file = 'general_mask.reg')
        IMP_main(input_image, 'reg_to_mask', region_file='general_mask.reg', user_interact = False)
        shutil.move('segm.fits','ini_segm.fits')
    
    if 3 in steps:
        # 3. Do overall frame sky subtraction    
        output_image = IMP_main(input_image, 'sky_correction', mask_image='general_mask.fits', output_image='sky_subtr.fits', sky_degree=5, user_interact = True) # NOTE: sky_degree!!!
    
    if 5 in steps or 6 in steps: # At the step 5 galaxy_ellipse_max.reg (based on r_lim) is created
        # 4. Determine galaxy ellipse
        xc_ell,yc_ell,sma_ell,smb_ell,PA_ell = IMP_main('sky_subtr.fits', 'determine_galaxy', xc=xc, yc=yc, mask_image='ini_segm.fits')

        # 5. Determine outskirts of the galaxy
        output_model, azim_tab = IMP_main('sky_subtr.fits', 'crea_azim_profile', xc=xc, yc=yc, sma=sma_ell, smb=smb_ell, PA=PA_ell)
        r_lim = IMP_main('sky_subtr.fits', 'plot_azim_profile', xc=xc, yc=yc, azim_tab='azim_model.txt', sma=sma_ell, smb=smb_ell, PA=PA_ell, region_file='galaxy_ellipse_max.reg', user_interact = False)

    if 6 in steps:
        # 6. Do galaxy sky subtraction (within an annulus) - more accurate 
        IMP_main('sky_subtr.fits', 'sky_galaxy', mask_image='ini_segm.fits', region_file='galaxy_ellipse_max.reg', user_interact = True, annulus_width=32.)#r_lim*0.414)

    if 7 in steps:
        # 7. Do cropping
        IMP_main('sky_subtr_galaxy.fits', 'crop_galaxy', region_file='galaxy_ellipse_max.reg')
        shutil.copy('sky_subtr_galaxy_crop.fits', 'galaxy.fits')

    if 8 in steps:
        # 8. Convert galaxy ellipse to the cropped image
        xc,yc,sma,smb,PA = crop_galaxy_image.read_region('galaxy_ellipse_max.reg')
        xc,yc = get_galaxy_center_ned.main('galaxy.fits', name=galaxy_name, RA=None, DEC=None)
        get_galaxy_ellipse.create_ellipse_region(xc, yc, sma, smb, PA, 'galaxy_ellipse_final.reg')

    if 10 in steps:    
        # 10. Do final masking: inner and outer
        IMP_main('galaxy.fits', 'final_masking', region_file='galaxy_ellipse_final.reg', psf_image='psf.fits', output_image='galaxy_clean.fits', mask_image='mask.fits') # 'galaxy_mask.reg' will be created

    if 11 in steps:
        #convert_reg_to_mask.mask('galaxy.fits', 'galaxy_mask.reg', output_image='galaxy_clean.fits', output_mask='mask.fits', mask_value=1, show_running=True, mask_DN=None)

        # 11. Prepare image for GALFIT
        xc,yc = get_galaxy_center_ned.main('galaxy.fits', name=galaxy_name, RA=None, DEC=None)
        shutil.copy('galaxy_clean.fits', 'galaxy_clean_galf.fits')
        m0,pix2sec = prep_for_galfit(xc, yc)
        
        # 12. Do GALFIT fitting
        dec_path = '/home/amosenko/Toshiba_1/CurrentWork/Rich/FINAL_SAMPLE/Decomposition/results/%s' % (galaxy_name)
        IMP_main('galaxy_clean_galf.fits', 'fitting', psf_image='psf.fits', mask_image='mask.fits', m0=m0, scale=pix2sec, out_directory=dec_path)




galaxies, directories = np.loadtxt('remaining_galaxies.dat', usecols=[0,1], dtype=str, unpack=True, skiprows = 0, delimiter='\t')

#f = open('log.txt', 'w')

for k in range(len(galaxies)):
  if k+1==38:
    print( '********GALAXY %s********' % (galaxies[k]) )
    galaxy_name = galaxies[k]
    directory = directories[k]
    main(galaxy_name, directory, input_image='new-image.fits', steps=[10,11])#,3,5,6,7,8])#[0,2,3,5,6,7,8,10,11]) # [7,8,10,11])
    #exit()