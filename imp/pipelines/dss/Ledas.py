#! /usr/bin/env python
#!/usr/bin/python
# -*- coding:  cp1251 -*-
import random as random_number
import sys
import math
import numpy as np
import re
from scipy import stats
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as patches
import matplotlib.path as path
from matplotlib.ticker import NullFormatter
from numpy import *
from pylab import *
import os
tmp_out = sys.stdout
from math import copysign
import urllib
import sys
from time import sleep
import random

from PIL import Image
import tarfile
import gzip


def untar(fname):
    if (fname.endswith("tar.gz")):
        tar = tarfile.open(fname, "r:gz")
        tar.extractall()
        tar.close()
    elif (fname.endswith("tar")):
        tar = tarfile.open(fname, "r:")
        tar.extractall()
        tar.close()
        
def unzip(file,outfilename):
    inF = gzip.open(file, 'rb')
    outF = open(outfilename, 'wb')
    outF.write( inF.read() )
    inF.close()
    outF.close()

def download(RA,DEC,name,mime='fits',survey='dss1',fieldsize='10x10',funits='min',remove_zp=True):
    print 'Downloading %s ...' % (name)
    pic = "http://www.ledas.ac.uk/cgi-bin/dss/dss_cgi.pl?coordinates=%f,%f&mime=%s&survey=%s&fieldsize=%s&funits=%s" % (RA/15.,DEC,mime,survey,fieldsize,funits)
    f = urllib.urlopen(pic)
    image = f.read()
    fout = open('./jpg/%s.%s.gz' % (name,mime), 'w')
    fout.write(image)
    fout.close()

    #untar('./jpg/%s.%s.gz' % (name,mime))
    try:
        unzip('./jpg/%s.%s.gz' % (name,mime),'./jpg/%s.%s' % (name,mime))
        if remove_zp==True:
            os.remove('./jpg/%s.%s.gz' % (name,mime))
    except:
        print '%s was not unzipped!' % (name)
    
if not os.path.exists('jpg/'):
        os.mkdir('jpg/')
'''
RA = 35.639224
DEC = 42.349146

name = 'NGC891_blue'
download(RA,DEC,name,mime='fits',survey='dss2blue',fieldsize='10x10',funits='min')
name = 'NGC891_red'
download(RA,DEC,name,mime='fits',survey='dss2red',fieldsize='10x10',funits='min')
name = 'NGC891_ir'
download(RA,DEC,name,mime='fits',survey='dss2ir',fieldsize='10x10',funits='min')
'''