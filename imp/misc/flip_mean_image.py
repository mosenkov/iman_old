#!/usr/bin/python
# DESCRIPTION:
# Script to flip and median image 
# MINIMAL USAGE: python flip_mean_image.py [input_image]
# EXAMPLE: python flip_mean_image.py HCG041_11Dec2018.fits
# NOTE: The image of the object should be centred! The major axis should be horizontal.

# Import the necessary modules
from astropy.io import fits as pyfits
import numpy as np
import argparse
import warnings

warnings.filterwarnings("ignore")

def main(input_image, output_image):
    hdulist = pyfits.open(input_image)
    data = hdulist[0].data
    ny,nx = np.shape(data)
    header = hdulist[0].header
    
    data1 = np.copy(data)
    
    data2 = np.fliplr(data)
    
    data3 = np.flipud(data)
    
    data4 = np.flipud(data2)
    
    #data_out = np.zeros((ny,nx))
    #for k in range(ny):
    #    for i in range(nx):
    #        data_out[k,i] = np.median([data1[k,i],data2[k,i],data3[k,i],data4[k,i]])
    
    data_out = np.median([data1,data2,data3,data4], axis=0)
    
    hdu = pyfits.PrimaryHDU(data_out, header)
    if output_image is None:
        output_image = input_image.split('.fits')[0]+'_%s.fits' % ('flipmean')
    hdu.writeto(output_image, clobber=True)        




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Flip-mean image")
    parser.add_argument("input_image", help="Input fits image")
    
    parser.add_argument("--o", help="Output fits image", type=str, default=None)  


    args = parser.parse_args()

    input_image = args.input_image
    output_image = args.o

    main(input_image, output_image=output_image)